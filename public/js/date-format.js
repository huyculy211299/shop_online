function dateFormat(date) {
    date = new Date(date);
    let year = date.getYear() - 100;
    let month = getNameMonth(date.getMonth() + 1);
    let day = date.getUTCDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;

    return day + ' ' + month + ' ' + year + ', ' + strTime;
}

function getNameMonth(month) {
    var nameMonth = '';
    switch (month) {
        case 1:
            nameMonth = 'Jan';
            break;
        case 2:
            nameMonth = 'Feb';
            break;
        case 3:
            nameMonth = 'Mar';
            break;
        case 4:
            nameMonth = 'Apr';
            break;
        case 5:
            nameMonth = 'May';
            break;
        case 6:
            nameMonth = 'June';
            break;
        case 7:
            nameMonth = 'July';
            break;
        case 8:
            nameMonth = 'August';
            break;
        case 9:
            nameMonth = 'September';
            break;
        case 10:
            nameMonth = 'October';
            break;
        case 11:
            nameMonth = 'November';
            break;
        default:
            nameMonth = 'December';
    }
    return nameMonth;
}