<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Client\HomeController@home')->name('client.home');
Route::get('/dang-ky-tai-khoan', function () {
    return view('client.register');
});
Route::get('/nhap-ten-tai-khoan', function () {
    return view('client.send_mail');
});
// chat box
Route::get('/getFormCustomerAdvisor', 'ChatBoxController@getFormCustomerAdvisor')->name('getFormCustomerAdvisor');
Route::get('/userAdvisor', 'ChatBoxController@userAdvisor')->name('userAdvisor');
// message account use
Route::post('/storetCustomer', 'ChatBoxController@store');
Route::get('/getListMessageUser', 'ChatBoxController@getMessage')->name('message');
Route::post('sendmessage', 'ChatBoxController@sendMessage');
Route::post('adminsendmessage', 'ChatBoxController@adminSendMessage');
Route::post('requestmessage', 'ChatBoxController@requestMessage');
Route::get('list-user-message', 'ChatBoxController@listUserMessage')->name('list-user-message');
// chat box


Route::get('/get-district-by-province_id/{id}', 'Client\AccountController@getDistrictByProvinceId')->name('client.getDistrictByProvinceId');
Route::post('/dang-ky-tai-khoan-post', 'Client\AccountController@addAccount')->name('client.addAccount');
Route::get('/list-order', function () {
    return view('client.order');
})->name('client.list-order');
Route::get('ForgotPassword', 'Client\AccountController@ForgotPassword')->name('client.ForgotPassword');
Route::get('/man-hinh-check-ma-otp/{email}', 'Client\AccountController@index_check_otp')->name('client.index_check_otp');
Route::get('/man-hinh-doi-mat-khau/{email}', 'Client\AccountController@index_change_password')->name('client.index_change_password');
Route::get('/Check-OTP', 'Client\AccountController@CheckOTP')->name('client.CheckOTP');
Route::post('/doi-mat-khau-voi-otp', 'Client\AccountController@ChangePasswordWithOTP')->name('client.ChangePasswordWithOTP');
Route::get('/order/{product_id}','Client\HomeController@orderUser')->name('client.order-user');
Route::post('/addOrder/{product_id}','Client\HomeController@addOrder')->name('client.order-add');
Route::get('/cart','Client\HomeController@addCart')->name('client.add-cart-user');

Route::get('/edit-color/{color_id}','Client\HomeController@editColor')->name('client.edit-color');
Route::get('/edit-color/{color_id}','Client\HomeController@editColor')->name('client.edit-color');

Route::post('/deleteCart/{cartId}','Client\HomeController@deleteCart')->name('client.delete-cart-user');
Route::get('/list-cart/{user_id}','Client\HomeController@listCart')->name('client.list-cart');
Route::get('/payment-cart/{cartId}','Client\HomeController@paymentCart')->name('client.payment-cart');
Route::get('/payment-cart-list','Client\HomeController@paymentCartList')->name('client.payment-cart-list');

Route::get('/update-cart/{cartId}','Client\HomeController@updateCart')->name('client.update-cart');
Route::post('/update-cart-store/{cartId}','Client\HomeController@updateCartStore')->name('client.update-cart-store');

Route::get('/list-order-by-user/{user_id}','Client\HomeController@listOrderByUser')->name('client.list-order-user');
Route::get('/edit-order-info/{user_id}','Client\HomeController@editOrderInfo')->name('client.edit-order-info');
Route::post('/update-order-info/{user_id}','Client\HomeController@updateOrderInfo')->name('client.update-order-info');


Route::post('/add-to-cart/{product_id}','Client\HomeController@addToCart')->name('client.add-to-cart');


Route::get('/search','Client\HomeController@search')->name('client.search');
Route::get('/search-page','Client\HomeController@searchPage')->name('client.search-page');
Route::get('/get-list-cate/{cate_id}','Client\HomeController@getListCate')->name('client.get-list-cate');
Route::get('/get-list-sale','Client\HomeController@getListSale')->name('client.get-list-sale');

Route::post('/addOrderUser','Client\HomeController@addOrderUser')->name('client.add-order');

Route::get('/buy-product', function () {
    return view('client.buy-product');
});
Route::get('/product-detail/{product_id}', 'Client\HomeController@productDetail')->name('client.product-detail');
Auth::routes();

Route::get('/list-new','Client\HomeController@listNew')->name('client.list-new');
Route::get('/detail-new/{id}','Client\HomeController@detailNew')->name('client.detail-new');



Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/home/updateUser/{id}', 'HomeController@UpdateUser')->name('UpdateUser')->middleware('auth');
Route::post('/home/editUser/{id}', 'HomeController@editUser')->name('editUser')->middleware('auth');
Route::get('/home/changePassword/{id}', 'HomeController@changePassword')->name('changePassword')->middleware('auth');
Route::post('/home/ChangePasswordUser/{id}', 'HomeController@ChangePasswordUser')->name('ChangePasswordUser')->middleware('auth');
Route::get('/district', 'Admin\DistrictController@index')->name('district.index')->middleware('auth');
Route::get('/product', 'Admin\ProductController@index')->name('product.index')->middleware('auth');
Route::get('/product/create', 'Admin\ProductController@create')->name('product.create')->middleware('auth');
Route::get('/product/update/{id}', 'Admin\ProductController@update')->name('product.update')->middleware('auth');
Route::get('/product/delete/{id}', 'Admin\ProductController@delete')->name('product.delete')->middleware('auth');
Route::post('/product/store', 'Admin\ProductController@store')->name('product.store')->middleware('auth');
Route::post('/product/edit/{id}', 'Admin\ProductController@edit')->name('product.edit')->middleware('auth');
Route::get('/product/update_hot/{id}', 'Admin\ProductController@update_hot')->name('product.update_hot')->middleware('auth');
Route::get('/product/update_sale/{id}', 'Admin\ProductController@update_sale')->name('product.update_sale')->middleware('auth');
Route::post('/product/create_product_detail/{id}', 'Admin\ProductController@create_product_detail')->name('product.create_product_detail')->middleware('auth');
Route::get('/product/detail/{id}', 'Admin\ProductController@detail')->name('product.detail')->middleware('auth');
Route::get('/product/delete_detail/{id}', 'Admin\ProductController@delete_detail')->name('product.delete_detail')->middleware('auth');
Route::post('/product/update_product_detail/{id}', 'Admin\ProductController@update_product_detail')->name('product.update_product_detail')->middleware('auth');


Route::get('/Order', 'Admin\OrderController@index')->name('order.index')->middleware('auth');
Route::get('/Order/create', 'Admin\OrderController@create')->name('order.create')->middleware('auth');
Route::get('/Order/update/{id}', 'Admin\OrderController@update')->name('order.update')->middleware('auth');
Route::get('/Order/delete/{id}', 'Admin\OrderController@delete')->name('order.delete')->middleware('auth');
Route::post('/Order/store', 'Admin\OrderController@store')->name('order.store')->middleware('auth');
Route::post('/Order/edit/{id}', 'Admin\OrderController@edit')->name('order.edit')->middleware('auth');
Route::get('/Order/check_order/{id}', 'Admin\OrderController@check_order')->name('order.check_order')->middleware('auth');



Route::get('/Category', 'Admin\CategoryController@index')->name('category.index')->middleware('auth');
Route::get('/Category/create', 'Admin\CategoryController@create')->name('category.create')->middleware('auth');
Route::get('/Category/update/{id}', 'Admin\CategoryController@update')->name('category.update')->middleware('auth');
Route::get('/Category/delete/{id}', 'Admin\CategoryController@delete')->name('category.delete')->middleware('auth');
Route::post('/Category/store', 'Admin\CategoryController@store')->name('category.store')->middleware('auth');
Route::post('/Category/edit/{id}', 'Admin\CategoryController@edit')->name('category.edit')->middleware('auth');
Route::get('/Category/Detail/{id}', 'Admin\CategoryController@detail')->name('category.detail')->middleware('auth');
Route::post('/Category/store_category', 'Admin\CategoryController@store_category')->name('category.store_category')->middleware('auth');


Route::get('/Post', 'Admin\PostController@index')->name('post.index')->middleware('auth');
Route::get('/Post/create', 'Admin\PostController@create')->name('post.create')->middleware('auth');
Route::post('/Post/store', 'Admin\PostController@store')->name('post.store')->middleware('auth');
Route::get('/Post/update/{id}', 'Admin\PostController@update')->name('post.update')->middleware('auth');
Route::post('/Post/edit/{id}', 'Admin\PostController@edit')->name('post.edit')->middleware('auth');
Route::get('/Post/delete/{id}', 'Admin\PostController@delete')->name('post.delete')->middleware('auth');


Route::get('/User', 'Admin\UserController@index')->name('user.index')->middleware('auth');
Route::get('/User/create', 'Admin\UserController@create')->name('user.create')->middleware('auth');
Route::post('/User/store', 'Admin\UserController@store')->name('user.store')->middleware('auth');
Route::get('/User/update/{id}', 'Admin\UserController@update')->name('user.update')->middleware('auth');
Route::post('/User/edit/{id}', 'Admin\UserController@edit')->name('user.edit')->middleware('auth');
Route::get('/User/delete/{id}', 'Admin\UserController@delete')->name('user.delete')->middleware('auth');
Route::get('/User/resetPassword/{id}', 'Admin\UserController@resetPassword')->name('user.resetPassword')->middleware('auth');
