<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatUser extends Model
{
    //
    protected $table  = 'chat_users';
    protected $fillable =[
        'name',
        'email',
        'phone',
        'password',
    ];
    protected $hidden = array('password');
}