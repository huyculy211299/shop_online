<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatUserMessage extends Model
{
    //
    protected $table = 'chat_user_messages';
    public static function getTotalMessage($user_id){
        return ChatUserMessage::where(['user_chat_id' => $user_id,'is_read' => 0])->count();
    }
}
