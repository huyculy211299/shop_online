<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatUserRoom extends Model
{
    //
    protected $table  = 'chat_user_rooms';
}
