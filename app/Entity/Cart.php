<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table = 'cart';
    public static function getTotalCart($user_id){
        return Cart::where('user_id',$user_id)->where('status',1)->get();
    }
}
