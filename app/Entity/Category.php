<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category';
    public static function getListCateParent(){
        return Category::where('parent',0)->where('is_active',1)->orderby('name','asc')->get();
    }
    public static function getCategoryParent($cate_id){
        return Category::where('parent',$cate_id)->where('is_active',1)->get();
    }
}
