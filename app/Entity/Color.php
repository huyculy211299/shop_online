<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    //
    protected $table = 'color';
    public static function  getColorById($color_id){
        return Color::where('id',$color_id)->first();
    }
}
