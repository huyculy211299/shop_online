<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    //
    protected $table = 'province';
    public static function getProvinceById($province_id){
        return Province::where('province_id',$province_id)->first();
    }
}
