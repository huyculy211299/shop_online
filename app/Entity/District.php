<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    //
    protected $table = 'district';
    public static function getDistrictById($district_id){
        return District::where('district_id',$district_id)->first();
    }
}
