<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    //
    protected $table = 'order_detail';
    public static function getProductDetailOrder($product_detail_id){
        return Product_detail::where('id',$product_detail_id)->first();
    }

}
