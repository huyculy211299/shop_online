<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product_detail extends Model
{
    //
    protected $table = 'product_detail';
    public static function getColorByProductId($product_id){
        return Product_detail::select('color')->where('product_id',$product_id)->distinct()->get();
    }
    public static function getSizeByProductId($product_id){
        return Product_detail::select('size')->where('product_id',$product_id)->distinct()->orderby('id','desc')->get();
    }
}
