<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'product';
    public static function getProductHot(){
        return Product::where('is_hot',1)->where('is_active',1)->orderby('id','desc')->limit(5)->get();
    }
    public static function getProductSale(){
        return Product::where('is_sale',1)->where('is_active',1)->orderby('id','desc')->limit(5)->get();
    }
    public static function getProductById($product_id){
        return Product::where('id',$product_id)->where('is_active',1)->first();
    }
}
