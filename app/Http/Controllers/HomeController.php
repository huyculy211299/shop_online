<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Product_detail;
use App\Entity\Post;
use App\Entity\Order;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ls_order = Order::where('is_active',1)->get();
        $ls_product = Product::where('is_active',1)->get();
        $ls_post = Post::where('is_active',1)->get();
        $data = array(
            'ls_order' => count($ls_order),
            'ls_product' => count($ls_product),
            'ls_post' => count($ls_post)
        );
        return view('admin.dashboard', compact('data'));
    }

    public function changePassword($id){
        $user = User::select('*')
        ->where('id',$id)->first();
        return view('admin.change_password', compact('user'));
    }

    public function UpdateUser($id){
        $user = User::select('*')
        ->where('id',$id)->first();
        return view('admin.update', compact('user'));
    }
    public function editUser(Request $request,$id){
        $this->validate($request,[
                'name' => 'required',
                'phone' => 'required|numeric',
                'email' => 'email',
            ],
            [
                'name.required' => 'Vui lòng nhập họ và tên',
                'phone.required' => 'Vui lòng nhập số điện thoại',
                'phone.numeric' => 'Vui lòng nhập đúng số điện thoại',
                'email.email' => 'Vui lòng nhập đúng định dạng mail',
            ]
        );
        $check_mail = User::where([['id','<>',$id],['email',$request->input('email')],['is_active',1]])->first();
        if($check_mail != null){
            $request->session()->flash('error', 'Email đã tồn tại!');
            return redirect()->back();
        }
        $check_phone = User::where([['id','<>',$id],['phone',$request->input('phone')],['is_active',1]])->first();
        if($check_phone != null){
            $request->session()->flash('error', 'Số điện thoại đã tồn tại!');
            return redirect()->back();
        }
        $update = User::where('id',$id)->update([
            'name'  => $request->input('name'),
            'phone' =>  $request->input('phone'),
            'address' =>  $request->input('address'),
            'email' =>  $request->input('email'),
            'updated_at' =>  date('Y-m-d H:i:s')
        ]);
        $request->session()->flash('success', 'Cập nhật tài khoản thành công!');
        return redirect()->back();
    }
    public function ChangePasswordUser(Request $request,$id){
        $this->validate($request,[
                'current_password' => 'required',
                'password' => 'required|min:6',
                'new_password' => 'required',
            ],
            [
                'current_password.required' => 'Vui lòng nhập mật khẩu cũ',
                'password.required' => 'Vui lòng nhập mật khẩu mới',
                'password.min' => 'Vui lòng nhập mật khẩu mới lớn hơn 6 ký tự',
                'new_password.required' => 'Vui lòng nhập lại mật khẩu mới',
            ]
        );
        if($request->input('password') != $request->input('new_password')){
            $request->session()->flash('error', 'Vui lòng nhập lại đúng mật khẩu mới!');
            return redirect()->back();
        }
        $user = User::where('id',$id)->first();
        if(Hash::check($request->input('current_password'),$user->password)){
            $update = User::where('id',$id)->update([
                'password'  => Hash::make($request->input('password'))
            ]);
            $request->session()->flash('success', 'Đổi mật khẩu thành công!');
            return redirect()->back();
        }
        $request->session()->flash('error', 'Nhập mật khẩu cũ không đúng!');
        return redirect()->back();
    }
}
