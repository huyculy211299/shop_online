<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Entity\District;
use App\Entity\Province;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Product_detail;
use App\Entity\Order;
use App\Entity\Order_detail;

class OrderController extends Controller
{
    //
    public function index(Request $request){
        $listOrder = Order::select('order.*','p.province_name','d.district_name','u.name as user_name','s.name as staff_name')
        ->leftjoin('province as p','p.province_id','order.province_id')
        ->leftjoin('district as d','d.district_id','order.district_id')
        ->leftjoin('users as u','u.id','order.user_id')
        ->leftjoin('users as s','s.id','order.staff_id')
        ->where('order.is_active',1);
        
        if (!empty($request->input('name'))) {
            $name = $request->input('name');
            $listOrder = $listOrder->where('order.name','like', '%' . $name . '%');
        }
        if (!empty($request->input('phone'))) {
            $phone = $request->input('phone');
            $listOrder = $listOrder->where('order.phone','like', '%' . $phone . '%');
        }
        if (!empty($request->input('type_buy'))) {
            $type_buy = $request->input('type_buy');
            $listOrder = $listOrder->where('order.type_buy', $type_buy);
        }
        if (!empty($request->input('from_date'))) {
            $from_date = $request->input('from_date');
            $listOrder = $listOrder->where('order.created_at','>=', $from_date);
        }
        if (!empty($request->input('to_date'))) {
            $to_date = $request->input('to_date');
            $listOrder = $listOrder->where('order.created_at','<=', date('Y-m-d H:i:s',strtotime($to_date)+24*3600 - 1));
        }
        if (!empty($request->input('category_id'))) {
            $category_id = $request->input('category_id');
            $listOrderDetail = Order_detail::select('p.name','pd.size','order_detail.order_id')
                                            ->leftjoin('product_detail as pd','pd.id','order_detail.product_detail_id')
                                            ->leftjoin('product as p','p.id','pd.product_id')
                                            ->where('p.category_id',$category_id)->distinct('order_detail.order_id')->get();
            // dd($listOrderDetail);
            $arr = [];
            foreach($listOrderDetail as $ls){
                $arr[] = $ls->order_id;
            }
            $listOrder = $listOrder->whereIN('order.id', $arr);

        }
        // $ls = $listOrder;
        $total_money = $listOrder->sum('order.total_money');
        // $total_money = 0;

        $listOrder = $listOrder->orderby('order.id','desc')->paginate(12);
        $category = Category::select('category.*','c.name as parent_name')->leftjoin('category as c','c.id','category.parent')->where('category.parent','!=',0)->get();
        return view('admin.order.index', compact('listOrder','category','total_money'));
    }

    public function create(){
        $category = Category::where('parent','!=',0)->get();
        $product  = Product_detail::select('product_detail.*','p.name as product_name','p.barcode')->leftjoin('product as p','p.id','product_detail.product_id')->where([['p.is_active',1],['p.status',1]])->get();
        return view('admin.order.create', compact('category','product'));
    }

    public function update($id){
        $order = Order::where('id',$id)->first();
        $category = Category::where('parent','!=',0)->get();
        $product  = Product_detail::select('product_detail.*','p.name as product_name','p.barcode')->leftjoin('product as p','p.id','product_detail.product_id')->where([['p.is_active',1],['p.status',1]])->get();
        return view('admin.order.update', compact('category','product','order'));
    }

    public function store(Request $request){
        $this->validate($request,[
                'name' => 'required',
                'phone' => 'required|numeric',
                'email' => 'email',
            ],
            [
                'name.required' => 'Vui lòng nhập tên khách hàng',
                'phone.required' => 'Vui lòng nhập số điện thoại',
                'phone.numeric' => 'Vui lòng nhập đúng số điện thoại',
                'email.email' => 'Vui lòng nhập đúng định dạng mail',
            ]
        );
        if($request->input('product_id')== null){
            $request->session()->flash('error', 'Vui lòng chọn sản phẩm!');
            return redirect()->back();
        }
        // dd($request->all());
        $ls_product = Product_detail::select('product_detail.*','p.price')->leftjoin('product as p','p.id','product_detail.product_id')
        ->whereIn('product_detail.id',$request->input('product_id'));
        // dd($request->file('image'));
        $id = Order::insertGetId([
            'name'  => $request->input('name'),
            'phone' =>  $request->input('phone'),
            'address' =>  $request->input('address'),
            'email' =>  $request->input('email'),
            'staff_id' =>  Auth::user()->id,
            'total_money' =>  $ls_product->sum('price'),
            'created_at' =>  date('Y-m-d H:i:s'),
            'status'    => 1,
            'type_buy' =>  2
        ]);
        $ls_product = $ls_product->get();
        foreach($ls_product as $ls){
            $create = Order_detail::insert([
                'order_id'  => $id,
                'product_detail_id' => $ls->id,
                'total' => 1,
                'money' => $ls->price,
                'created_at'    => date('Y-m-d H:i:s')
            ]);
        }
        $request->session()->flash('success', 'Thêm mới đơn hàng thành công!');
        $url = redirect()->route('order.index')->getTargetUrl();
        return redirect($url);
    }

    public function create_product_detail(Request $request,$id){
        $p_id = Product_detail::insertGetId([
            'product_id'  => $id,
            'size' =>  $request->input('size'),
            'total_product' =>  $request->input('total_product'),
            'created_at' =>  date('Y-m-d H:i:s')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
			
			// Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Product_detail::where('id',$p_id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Thêm mới thành công!');
        $url = redirect()->route('product.index')->getTargetUrl();
        return redirect($url);
    }

    public function edit(Request $request,$id){
        $this->validate($request,[
                'name' => 'required',
                'phone' => 'required|numeric',
                'email' => 'email',
            ],
            [
                'name.required' => 'Vui lòng nhập tên khách hàng',
                'phone.required' => 'Vui lòng nhập số điện thoại',
                'phone.numeric' => 'Vui lòng nhập đúng số điện thoại',
                'email.email' => 'Vui lòng nhập đúng định dạng mail',
            ]
        );
        if($request->input('product_id') == null){
            $request->session()->flash('error', 'Vui lòng chọn sản phẩm!');
            return redirect()->back();
        }
        $ls_product = Product_detail::select('product_detail.*','p.price')->leftjoin('product as p','p.id','product_detail.product_id')
        ->whereIn('product_detail.id',$request->input('product_id'));
        // dd($request->file('image'));
        $update = Order::where('id',$id)->update([
            'name'  => $request->input('name'),
            'phone' =>  $request->input('phone'),
            'address' =>  $request->input('address'),
            'email' =>  $request->input('email'),
            'staff_id' =>  Auth::user()->id,
            'total_money' =>  $ls_product->sum('price'),
            'delivery_status'    => $request->input('delivery_status') != null ? $request->input('delivery_status') : 1,
            'updated_at' =>  date('Y-m-d H:i:s')
        ]);
        $ls_product = $ls_product->get();
        if(count($ls_product) > 0){
            $delete = Order_detail::where('order_id',$id)->delete();
            foreach($ls_product as $ls){
                $create = Order_detail::insert([
                    'order_id'  => $id,
                    'product_detail_id' => $ls->id,
                    'total' => 1,
                    'money' => $ls->price,
                    'created_at'    => date('Y-m-d H:i:s')
                ]);
            }
        }
       
        $request->session()->flash('success', 'Cập nhật sản phẩm thành công!');
        $url = redirect()->route('order.index')->getTargetUrl();
        return redirect($url);
    }

    public function delete(Request $request,$id){
        $delete = Order::where('id',$id)->update([
            'is_active' => 0
        ]);
        $request->session()->flash('success', 'Hủy đơn hàng thành công!');
        $url = redirect()->route('order.index')->getTargetUrl();
        return redirect($url);
    }

    public function check_order(Request $request,$id){
        $delete = Order::where('id',$id)->update([
            'status' => 1
        ]);
        $request->session()->flash('success', 'Duyệt đơn hàng thành công!');
        $url = redirect()->route('order.index')->getTargetUrl();
        return redirect($url);
    }

    public function update_hot(Request $request,$id){
        $check = Product::where('id',$id)->first();
        if($check->is_hot == 1){
            $update = Product::where('id',$id)->update([
                'is_hot' => 0
            ]);
            $request->session()->flash('success', 'Tắt HOT sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
        else{
            $update = Product::where('id',$id)->update([
                'is_hot' => 1
            ]);
            $request->session()->flash('success', 'Bật HOT sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
    }
    public function update_sale(Request $request,$id){
        $check = Product::where('id',$id)->first();
        if($check->is_sale == 1){
            $update = Product::where('id',$id)->update([
                'is_sale' => 0
            ]);
            $request->session()->flash('success', 'Tắt Sale sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
        else{
            $update = Product::where('id',$id)->update([
                'is_sale' => 1
            ]);
            $request->session()->flash('success', 'Bật Sale sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
    }

}
