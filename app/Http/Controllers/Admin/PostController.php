<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\District;
use App\Entity\Province;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Product_detail;
use App\Entity\Post;
use Illuminate\Support\Facades\Auth;
class PostController extends Controller
{
    //
    public function index(Request $request){
        $post = Post::select('post.*','u.name as staff_name')->where('post.is_active',1)->leftjoin('users as u','u.id','post.user_id');
        if (!empty($request->input('title'))) {
            $title = $request->input('title');
            $post = $post->where('title','like', '%' . $title . '%');
        }
        // if (!empty($request->input('is_sale'))) {
        //     $is_sale = $request->input('is_sale');
        //     $listProduct = $listProduct->where('product.is_sale',$is_sale);
        // }
        // if (!empty($request->input('is_hot'))) {
        //     $is_hot = $request->input('is_hot');
        //     $listProduct = $listProduct->where('product.is_hot',$is_hot);
        // }
        // if (!empty($request->input('category_id'))) {
        //     $category_id = $request->input('category_id');
        //     $listProduct = $listProduct->where('product.is_hot',$category_id);
        // }
        $post = $post->paginate(12);
        return view('admin.post.index', compact('post'));
    }

    public function detail(Request $request,$id){
        $category = Category::where('parent',$id)->get();
        return view('admin.category.list_category', compact('category'));
    }

    public function create(){
        return view('admin.post.create');
    }

    public function update($id){
        $post = Post::where('id',$id)->first();
        return view('admin.post.update', compact('post'));
    }

    public function store(Request $request){
        
        $this->validate($request,[
                'title' => 'required',
            ],
            [
                'title.required' => 'Vui lòng nhập tiêu đề bài viết',
            ]
        );
        // dd($request->file('image'));
        $id = Post::insertGetId([
            'user_id'   => Auth::user()->id,
            'title'  => $request->input('title'),
            'content' =>  $request->input('content'),
            'created_at' =>  date('Y-m-d H:i:s'),
            'type' =>  $request->input('status')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
            
            // Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Post::where('id',$id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Thêm bài viết thành công!');
        $url = redirect()->route('post.index')->getTargetUrl();
        return redirect($url);
    }

    public function edit(Request $request,$id){
        
        // dd($request->file('image'));
        $this->validate($request,[
                'title' => 'required',
            ],
            [
                'title.required' => 'Vui lòng nhập tiêu đề bài viết',
            ]
        );
        // dd($request->file('image'));
        $update = Post::where('id',$id)->update([
            'title'  => $request->input('title'),
            'content' =>  $request->input('content'),
            'updated_at' =>  date('Y-m-d H:i:s'),
            'type' =>  $request->input('status')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
            
            // Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Post::where('id',$id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Cập nhật bài viết thành công!');
        $url = redirect()->route('post.index')->getTargetUrl();
        return redirect($url);
    }

    public function store_category(Request $request){
        
        // dd($request->file('image'));
        $id = Category::insertGetId([
            'name'  => $request->input('name'),
            'created_at' =>  date('Y-m-d H:i:s'),
            'parent' =>  $request->input('category_id')
        ]);
        $request->session()->flash('success', 'Thêm mới danh mục con thành công!');
        $url = redirect()->route('category.index')->getTargetUrl();
        return redirect($url);
    }

    public function create_product_detail(Request $request,$id){
        $p_id = Product_detail::insertGetId([
            'product_id'  => $id,
            'size' =>  $request->input('size'),
            'total_product' =>  $request->input('total_product'),
            'created_at' =>  date('Y-m-d H:i:s')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
			
			// Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Product_detail::where('id',$p_id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Thêm mới thành công!');
        $url = redirect()->route('product.index')->getTargetUrl();
        return redirect($url);
    }

    public function edit_parent(Request $request,$id){
        $this->validate($request,[
                'name' => 'required',
            ],
            [
                'name.required' => 'Vui lòng nhập tên sản phẩm',
            ]
        );
        // dd($request->file('image'));
        $update = Product::where('id',$id)->update([
            'name'  => $request->input('name'),
            'description' =>  $request->input('description'),
            'company' =>  $request->input('company'),
            'price' =>  $request->input('price'),
            'is_hot' =>  $request->input('is_hot'),
            'is_sale' =>  $request->input('is_sale'),
            'updated_at' =>  date('Y-m-d H:i:s'),
            'percent_sale' =>  $request->input('percent_sale'),
            'category_id' =>  $request->input('category_id'),
            'status' =>  $request->input('status')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
			
			// Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Product::where('id',$id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Cập nhật sản phẩm thành công!');
        $url = redirect()->route('product.index')->getTargetUrl();
        return redirect($url);
    }

    public function delete(Request $request,$id){
        $delete = Post::where('id',$id)->delete();
        $request->session()->flash('success', 'Xóa bài viết thành công!');
        return redirect()->back();
    }

    public function update_hot(Request $request,$id){
        $check = Product::where('id',$id)->first();
        if($check->is_hot == 1){
            $update = Product::where('id',$id)->update([
                'is_hot' => 0
            ]);
            $request->session()->flash('success', 'Tắt HOT sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
        else{
            $update = Product::where('id',$id)->update([
                'is_hot' => 1
            ]);
            $request->session()->flash('success', 'Bật HOT sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
    }
    public function update_sale(Request $request,$id){
        $check = Product::where('id',$id)->first();
        if($check->is_sale == 1){
            $update = Product::where('id',$id)->update([
                'is_sale' => 0
            ]);
            $request->session()->flash('success', 'Tắt Sale sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
        else{
            $update = Product::where('id',$id)->update([
                'is_sale' => 1
            ]);
            $request->session()->flash('success', 'Bật Sale sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
    }

}
