<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\District;
use App\Entity\Province;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Product_detail;
use App\Entity\Color;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    //
    public function index(Request $request){
        $listProduct = Product::select('product.*','c.name as category_name')
        ->leftjoin('category as c','c.id','product.category_id')
        ->where('product.is_active',1);
        if (!empty($request->input('name'))) {
            $name = $request->input('name');
            $listProduct = $listProduct->where('product.name','like', '%' . $name . '%');
        }
        if (!empty($request->input('is_sale'))) {
            $is_sale = $request->input('is_sale');
            $listProduct = $listProduct->where('product.is_sale',$is_sale);
        }
        if (!empty($request->input('is_hot'))) {
            $is_hot = $request->input('is_hot');
            $listProduct = $listProduct->where('product.is_hot',$is_hot);
        }
        if (!empty($request->input('category_id'))) {
            $category_id = $request->input('category_id');
            $listProduct = $listProduct->where('product.is_hot',$category_id);
        }
        if (!empty($request->input('from_date'))) {
            $from_date = $request->input('from_date');
            $listProduct = $listProduct->where('product.created_at','>=', $from_date);
        }
        if (!empty($request->input('to_date'))) {
            $to_date = $request->input('to_date');
            $listProduct = $listProduct->where('product.created_at','<=', date('Y-m-d H:i:s',strtotime($to_date)+24*3600 - 1));
        }
        $listProduct = $listProduct->orderby('product.id','desc')->paginate(12);
        $category = Category::where('parent','!=',0)->get();
        $color = Color::get();
        return view('admin.product.index', compact('listProduct','category','color'));
    }

    public function create(){
        $category = Category::select('category.*','c.name as parent_name')->leftjoin('category as c','c.id','category.parent')->where('category.parent','!=',0)->get();
        return view('admin.product.create', compact('category'));
    }

    public function update($id){
        $product = Product::select('product.*','c.name as category_name')->leftjoin('category as c','c.id','product.category_id')->where('product.id',$id)->first();
        $category = Category::select('category.*','c.name as parent_name')->leftjoin('category as c','c.id','category.parent')->where('category.parent','!=',0)->get();
        return view('admin.product.update', compact('category','product'));
    }

    public function detail($id){
        $product = Product::select('product.*','c.name as category_name')->leftjoin('category as c','c.id','product.category_id')->where('product.id',$id)->first();
        $category = Category::where('parent','!=',0)->get();
        $product_detail = Product_detail::select('product_detail.*','c.name as color_name')->leftjoin('color as c','c.id','product_detail.color')->where('product_detail.product_id',$id)->get();
        $color = Color::get();
        return view('admin.product.detail', compact('category','product','product_detail','color'));
    }

    public function store(Request $request){
        $this->validate($request,[
                'name' => 'required',
                'barcode' => 'required'
            ],
            [
                'name.required' => 'Vui lòng nhập tên sản phẩm',
                'barcode.required' => 'Vui lòng nhập mã sản phẩm',
            ]
        );
        if($request->input('category_id') == null){
            $request->session()->flash('error', 'Vui lòng chọn danh mục sản phẩm!');
            return redirect()->back();
        }
        // dd($request->file('image'));
        $id = Product::insertGetId([
            'name'  => $request->input('name'),
            'barcode'  => $request->input('barcode'),
            'description' =>  $request->input('description'),
            'company' =>  $request->input('company'),
            'price' =>  $request->input('price'),
            'is_hot' =>  $request->input('is_hot'),
            'is_sale' =>  $request->input('is_sale'),
            'created_at' =>  date('Y-m-d H:i:s'),
            'percent_sale' =>  $request->input('percent_sale'),
            'category_id' =>  $request->input('category_id'),
            'status' =>  $request->input('status')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
			
			// Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Product::where('id',$id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Thêm mới sản phẩm thành công!');
        $url = redirect()->route('product.index')->getTargetUrl();
        return redirect($url);
    }

    public function create_product_detail(Request $request,$id){
        $p_id = Product_detail::insertGetId([
            'product_id'  => $id,
            'size' =>  $request->input('size'),
            'color' =>  $request->input('color'),
            'total_product' =>  $request->input('total_product'),
            'created_at' =>  date('Y-m-d H:i:s')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
			
			// Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Product_detail::where('id',$p_id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Thêm mới thành công!');
        return redirect()->back();
    }

    public function update_product_detail(Request $request,$id){
        $update = Product_detail::where('id',$id)->update([
            'size' =>  $request->input('size'),
            'color' =>  $request->input('color'),
            'total_product' =>  $request->input('total_product'),
            'updated_at' =>  date('Y-m-d H:i:s')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
			
			// Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Product_detail::where('id',$id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Cập nhật thành công!');
        return redirect()->back();
    }

    public function edit(Request $request,$id){
        $this->validate($request,[
                'name' => 'required',
            ],
            [
                'name.required' => 'Vui lòng nhập tên sản phẩm',
            ]
        );
        if($request->input('category_id') == null){
            $request->session()->flash('error', 'Vui lòng chọn danh mục sản phẩm!');
            return redirect()->back();
        }
        // dd($request->file('image'));
        $update = Product::where('id',$id)->update([
            'name'  => $request->input('name'),
            'description' =>  $request->input('description'),
            'company' =>  $request->input('company'),
            'price' =>  $request->input('price'),
            'is_hot' =>  $request->input('is_hot'),
            'is_sale' =>  $request->input('is_sale'),
            'updated_at' =>  date('Y-m-d H:i:s'),
            'percent_sale' =>  $request->input('percent_sale'),
            'category_id' =>  $request->input('category_id'),
            'status' =>  $request->input('status')
        ]);
        if($request->file('image') != null){
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fileName = md5(time()).'.'.$fileExtension;
            $uploadPath = public_path('/upload'); // Thư mục upload
			
			// Bắt đầu chuyển file vào thư mục
            $request->file('image')->move($uploadPath, $fileName);
            // dd("đã vào");
            $update = Product::where('id',$id)->update([
                'image' => '/upload/'.$fileName
            ]);
        }
        $request->session()->flash('success', 'Cập nhật sản phẩm thành công!');
        $url = redirect()->route('product.index')->getTargetUrl();
        return redirect($url);
    }

    public function delete(Request $request,$id){
        $delete = Product::where('id',$id)->update([
            'is_active' => 0
        ]);
        $request->session()->flash('success', 'Xóa sản phẩm thành công!');
        $url = redirect()->route('product.index')->getTargetUrl();
        return redirect($url);
    }

    public function delete_detail(Request $request,$id){
        $delete = Product_detail::where('id',$id)->delete();
        $request->session()->flash('success', 'Xóa sản phẩm thành công!');
        return redirect()->back();
    }

    public function update_hot(Request $request,$id){
        $check = Product::where('id',$id)->first();
        if($check->is_hot == 1){
            $update = Product::where('id',$id)->update([
                'is_hot' => 0
            ]);
            $request->session()->flash('success', 'Tắt HOT sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
        else{
            $update = Product::where('id',$id)->update([
                'is_hot' => 1
            ]);
            $request->session()->flash('success', 'Bật HOT sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
    }
    public function update_sale(Request $request,$id){
        $check = Product::where('id',$id)->first();
        if($check->is_sale == 1){
            $update = Product::where('id',$id)->update([
                'is_sale' => 0
            ]);
            $request->session()->flash('success', 'Tắt Sale sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
        else{
            $update = Product::where('id',$id)->update([
                'is_sale' => 1
            ]);
            $request->session()->flash('success', 'Bật Sale sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        }
    }

}
