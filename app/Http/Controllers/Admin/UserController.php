<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\District;
use App\Entity\Province;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Product_detail;
use App\Entity\Color;
use App\Entity\User;
use App\Entity\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail as SendEmail;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $listUser = User::select('users.*', 'r.name as role_name')
            ->leftjoin('role as r', 'r.id', 'users.role')
            ->where('users.is_active', 1);
        if (!empty($request->input('name'))) {
            $name = $request->input('name');
            $listUser = $listUser->where('users.name', 'like', '%' . $name . '%');
        }
        if (!empty($request->input('phone'))) {
            $phone = $request->input('phone');
            $listUser = $listUser->where('users.phone', 'like', '%' . $phone . '%');
        }
        if (!empty($request->input('role'))) {
            $role = $request->input('role');
            $listUser = $listUser->where('users.role', $role);
        }
        if (!empty($request->input('from_date'))) {
            $from_date = $request->input('from_date');
            $listUser = $listUser->where('users.created_at','>=', $from_date);
        }
        if (!empty($request->input('to_date'))) {
            $to_date = $request->input('to_date');
            $listUser = $listUser->where('users.created_at','<=', date('Y-m-d H:i:s',strtotime($to_date)+24*3600 - 1));
        }
        $listUser = $listUser->paginate(12);
        $ls_role = Role::get();
        return view('admin.user.index', compact('listUser', 'ls_role'));
    }

    public function create()
    {
        $ls_role = Role::get();
        return view('admin.user.create', compact('ls_role'));
    }
    public function update($id)
    {
        $user = User::select('*')
            ->where('id', $id)->first();
        $ls_role = Role::get();
        return view('admin.user.update', compact('ls_role', 'user'));
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'phone' => 'required|numeric|unique:users',
                'email' => 'email|unique:users',
                'password' => 'required|min:6'
            ],
            [
                'name.required' => 'Vui lòng nhập họ và tên',
                'phone.required' => 'Vui lòng nhập số điện thoại',
                'phone.numeric' => 'Vui lòng nhập đúng số điện thoại',
                'password.required' => 'Vui lòng nhập mật khẩu',
                'password.min' => 'Vui lòng nhập mật khẩu lớn hơn 6 ký tự',
                'phone.unique' => 'Số điện thoại đã tồn tại',
                'email.unique' => 'Email đã tồn tại',
                'email.email' => 'Vui lòng nhập đúng định dạng mail',
            ]
        );
        if ($request->input('role') == null) {
            $request->session()->flash('error', 'Vui lòng chọn loại tài khoản!');
            return redirect()->back();
        }


        // dd($request->all());
        // dd($request->file('image'));
        $id = User::insertGetId([
            'name'  => $request->input('name'),
            'phone' =>  $request->input('phone'),
            'address' =>  $request->input('address'),
            'email' =>  $request->input('email'),
            'role' => $request->input('role'),
            'password'  => Hash::make($request->input('password')),
            'created_at' =>  date('Y-m-d H:i:s')
        ]);
        $request->session()->flash('success', 'Thêm mới tài khoản thành công!');
        $url = redirect()->route('user.index')->getTargetUrl();
        return redirect($url);
    }
    public function edit(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'phone' => 'required|numeric',
                'email' => 'email',
            ],
            [
                'name.required' => 'Vui lòng nhập họ và tên',
                'phone.required' => 'Vui lòng nhập số điện thoại',
                'phone.numeric' => 'Vui lòng nhập đúng số điện thoại',
                'email.email' => 'Vui lòng nhập đúng định dạng mail',
            ]
        );
        if ($request->input('role') == null) {
            $request->session()->flash('error', 'Vui lòng chọn loại tài khoản!');
            return redirect()->back();
        }
        $check_mail = User::where([['id', '<>', $id], ['email', $request->input('email')], ['is_active', 1]])->first();
        if ($check_mail != null) {
            $request->session()->flash('error', 'Email đã tồn tại!');
            return redirect()->back();
        }
        $check_phone = User::where([['id', '<>', $id], ['phone', $request->input('phone')], ['is_active', 1]])->first();
        if ($check_phone != null) {
            $request->session()->flash('error', 'Số điện thoại đã tồn tại!');
            return redirect()->back();
        }


        // dd($request->all());
        // dd($request->file('image'));
        $update = User::where('id', $id)->update([
            'name'  => $request->input('name'),
            'phone' =>  $request->input('phone'),
            'address' =>  $request->input('address'),
            'email' =>  $request->input('email'),
            'role' => $request->input('role'),
            'updated_at' =>  date('Y-m-d H:i:s')
        ]);
        $request->session()->flash('success', 'Cập nhật tài khoản thành công!');
        $url = redirect()->route('user.index')->getTargetUrl();
        return redirect($url);
    }
    public function delete(Request $request, $id)
    {
        $delete = User::where('id', $id)->update([
            'is_active' => 0
        ]);
        $request->session()->flash('success', 'Xóa tài khoản thành công!');
        $url = redirect()->route('user.index')->getTargetUrl();
        return redirect($url);
    }

    public function resetPassword(Request $request, $id)
    {
        $delete = User::where('id', $id)->update([
            'password'  => Hash::make("123456"),
        ]);
        $request->session()->flash('success', 'Reset mật khẩu thành công!');
        $url = redirect()->route('user.index')->getTargetUrl();
        return redirect($url);
    }
   
}
