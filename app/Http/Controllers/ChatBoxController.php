<?php

namespace App\Http\Controllers;

use App\ChatUser;
use App\ChatUserMessage;
use App\ChatUserRoom;
use App\Entity\User;
use App\Events\ChatMessageCustomer;
use Carbon\Carbon;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log as FacadesLog;

class ChatBoxController extends Controller
{

    //
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = new ChatUser();
            // $user->role_id = 6;
            $user->email = $request->has('inputEmail') ? $request->inputEmail : '';
            $user->name = $request->has('inputFullName') ? $request->inputFullName : 'Chưa cập nhật';
            $user->phone = $request->has('inputPhonenumber') ? $request->inputPhonenumber : '';
            $user->password = '';

            $user->save();
            // return $user;

            $createChatMessage = new ChatUserMessage();
            $createChatMessage->customer_token = $request->_token;
            $createChatMessage->user_chat_id = $user->id;
            $createChatMessage->auth_id = $user->id;
            $createChatMessage->message = $request->inputContentQuestion;
            $createChatMessage->is_read = 0;
            $createChatMessage->save();

            $chat_room = new ChatUserRoom();
            $chat_room->chat_message_id = $createChatMessage->id;
            $chat_room->user_chat_id = $user->id;
            $chat_room->save();
            DB::commit();
            $chatRoom = [
                'author_id' =>  $user->id,
                'author_email' =>  $user->email,
                'author_name' =>   $user->name,
                'author_phone' =>  $user->phone,
                'message' =>  $createChatMessage->message,
                'created_at' =>  date('d-M-y, h:i', strtotime($createChatMessage->created_at)),
            ];
            return view('Customer_advisor.show-message', compact('chatRoom'));
        } catch (\Exception $e) {
            // DB::rollBack();
            FacadesLog::error($e->getMessage() . '   ' . $e->getLine());
        }
    }
    public function requestMessage( Request $request){
        // $user_chat_id = ChatUserMessage::where('customer_token',$request->_token)->first()->user_chat_id;

        $data = new ChatUserMessage();
        $data->customer_token = $request->_token;
        // $data->user_chat_id = $user_chat_id;
        $data->user_chat_id = $request->user_id;
        $data->auth_id = $request->user_id;
        $data->message = $request->message;
        $data->is_read = 0;
        $data->save();
        event($e = new ChatMessageCustomer($data));
    }
    
    public function listUserMessage(){
        // dd(1);
        $listUserMessage = ChatUserMessage::get();
        $listUser = ChatUser::orderby('id', 'desc')->get();

        return view('Customer_advisor.list-message-user', compact('listUser', 'listUserMessage'));
    }
    public function getFormCustomerAdvisor(){
        return view('Customer_advisor.create_form_chat');
    }
    public function getMessage()
    {

        // return request()->all();
        // $messages = chat::get();
        $user_chat_id = request()->user_chat_id;
        ChatUserMessage::where('user_chat_id',$user_chat_id)->update(['is_read' => 1]);
        $messages = ChatUserMessage::where('user_chat_id', $user_chat_id)->get();
        // return $messages;

        return view('Customer_advisor.show-list-message-user', compact('messages'));
    }
    public function adminSendMessage( Request $request){
        $data = new ChatUserMessage();
        $data->customer_token = $request->_token;
        $data->user_chat_id = $request->user_chat_id;
        $data->auth_id = Auth::id();
        $data->message = $request->message;
        $data->is_read = 1;
        $data->save();
        event($e = new ChatMessageCustomer($data));
    }
    public function userAdvisor(){
        $user = User::find(Auth::id());
        $now = Carbon ::now('Asia/Ho_Chi_Minh');
        // return $user;
        return view('Customer_advisor.message-use-account',compact('user','now'));
    }
}
