<?php

namespace App\Http\Controllers\client;

use App\Entity\Cart;
use App\Entity\Color;
use App\Entity\District;
use App\Entity\Order;
use App\Entity\Order_detail;
use App\Entity\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Product;
use App\Entity\Product_detail;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    //
    protected $product;
    protected $productDetail;
    protected $cart;
    protected $order;
    protected $order_detail;
    public function __construct(Product_detail $productDetail, Product $product, Cart $cart, Order $order, Order_detail $order_detail)
    {
        # code...
        $this->productDetail = $productDetail;
        $this->product = $product;
        $this->cart = $cart;
        $this->order = $order;
        $this->order_detail = $order_detail;
    }
    public function home()
    {
        # code...
        $listProduct = $this->product->where('is_active', 1)->get();
        // dd($listProduct);
        return view('client.home', compact('listProduct'));
    }
    public function productDetail($product_id)
    {
        # code...
        $product = $this->product->where('id', $product_id)->first();
        $detailProduct = $this->productDetail->where('product_id', $product_id)->get();
        $arr = [];
        foreach ($detailProduct as $d) {
            $arr[] = json_decode(json_encode($d), true);
        }
        // dd($detailProduct);
        $productDetail = [
            'id'        => $product->id,
            'name'        => $product->name,
            'is_sale'        => $product->is_sale,
            'percent_sale'        => $product->percent_sale,
            'price'        => $product->price,
            'description'        => $product->description,
            'image'        => $product->image,
            'productSize'        => $arr
        ];
        // dd($productDetail['productSize']);
        $productHot = $this->product->where('is_hot', 1)->where('is_active', 1)->get();
        $productSale = $this->product->where('is_sale', 1)->where('is_active', 1)->get();
        // foreach($productHot as $item){
        //     dd($item->image);
        // }
        // dd($productHot);
        return view('client.product-detail', compact('productDetail', 'productHot', 'productSale'));
    }
    public function orderUser($product_id)
    {
        # code...
        $product = $this->product->where('id', $product_id)->first();
        $detailProduct = $this->productDetail->where('product_id', $product_id)->get();
        $colorProduct = $this->productDetail->select('color')->where('product_id', $product_id)->distinct()->get();
        $arr = [];

        foreach ($detailProduct as $d) {
            $arr[] = json_decode(json_encode($d), true);
        }
        $listColor = DB::table('color')->get();
        $colors = [];
        $color_id = [];
        $colors = [];
        foreach ($listColor as $color) {
            $colors[$color->id] = $color;
        }
        $productDetail = [
            'id'        => $product->id,
            'name'        => $product->name,
            'price'        => $product->price,
            'description'        => $product->description,
            'image'        => $product->image,
            'is_sale'        => $product->is_sale,
            'percent_sale'        => $product->percent_sale,
            'productSize'        => $arr
        ];
        // dd($productDetail);
        return view('client.order-product', compact('productDetail', 'colors', 'colorProduct'));
    }
    public function addOrder(Request $request)
    {
        # code...
        try {
            $product_detail_id = Product_detail::where('size', $request->size_id)
                ->where('product_id', $request->productDetail_id)
                ->where('color', $request->color_id)
                ->first();

            $this->cart->product_detail_id = $product_detail_id->id;
            $this->cart->total = (int)$request->total;
            $this->cart->status = 1;
            $this->cart->user_id = isset(Auth::user()->id) ? Auth::user()->id : csrf_token();
            $this->cart->save();
            $cart = $this->cart->id;
            return [
                'message' => 200,
                'url' => route('client.list-cart', $this->cart->user_id),
            ];
        } catch (Exception $e) {
            return [
                'message' => 500,
            ];
        }
    }
    public function updateCartStore($cartId, Request $request){

        $cartEdit = $this->cart->find($cartId);
        $total_old = $cartEdit->total;

        $cartEdit->total = $request->total;
        $cartEdit->product_detail_id = $request->product_detail_id;

        $cartEdit->save();
        $productDetail = $this->productDetail->find($request->prodcut_detail_id);
        $productDetail->total_product = $productDetail->total_product - $request->total + $total_old;
        $productDetail->save();
        return [
            'message'   => 200,
            // 'url'       => route('client.list-order-user', Auth::user()->id),
        ];

    }
    public function addOrderUser(Request $request)
    {
        $totalMoney = 0;
        foreach($request->listCartId as $cartId){
            $cart = $this->cart->where('id',$cartId)->first();
            $cart->status = 2;
            $cart->save();
            $productCartDetail= $this->productDetail->where('id',$cart->product_detail_id)->first();
            $productCartDetail->total_product =  $productCartDetail->total_product - $cart->total;
            $productCartDetail->save();
            $product = $this->product->where('id',$productCartDetail->product_id)->first();
            if($product->is_sale == 1){
                $money_product = $cart->total * $product->price * (100 - $product->percent_sale )/100 ;
            }
            $totalMoney += $money_product;
            $listCart[] = [
                'cart'  => $cart,
                'productCartDetail'  => $productCartDetail,
                'product'  => $product,
                'money_product'  => $money_product,
            ];
        }
        $product = $this->product->where('id',$request->product_id)->first();


        $this->order->name = $request->name;
        $this->order->phone = $request->phone;
        $this->order->address = $request->address;
        $this->order->user_id = isset(Auth::user()->id) ? Auth::user()->id : 0;
        $this->order->email = isset(Auth::user()->email) ? Auth::user()->email : $request->_token;
        $this->order->province_id = $request->province_id;
        $this->order->district_id = $request->district_id;
        $this->order->status = 0;
        $this->order->is_active = 1;
        $this->order->type_buy = 1;
        $this->order->total_money =  $totalMoney;
        $this->order->save();
        $orderId = $this->order->id;
        // return $listCart;

        foreach($listCart as $cart){
            $insert = [
                'order_id' => $orderId,
                'total' => $cart['cart']->total,
                'product_detail_id' => $cart['productCartDetail']->id,
                'money' => $cart['money_product'],
                'created_at' => Carbon::now('utc'),
                'updated_at' => Carbon::now('utc'),
            ];
            DB::table('order_detail')->insert($insert);
            // $this->order_detail->order_id = $orderId;
            // $this->order_detail->total = $cart['cart']->total;
            // $this->order_detail->product_detail_id = $cart['productCartDetail']->id;
            // $this->order_detail->money = $cart['money_product'];
            // $this->order_detail->save();
        }
        if (Auth::check()) {
            return [
                'message'   => 200,
                'url'       => route('client.list-order-user', Auth::user()->id),
            ];
        } else {
            return [
                'message'   => 200,
                'url'       => route('client.list-cart', csrf_token()),

            ];
        }


        try {





            $this->order_detail->order_id = $orderId;
            $this->order_detail->total = $request->total;
            $this->order_detail->product_detail_id = $request->id;
            $this->order_detail->money = $request->money;
            $this->order_detail->save();
            // $cart = $this->cart->find($request->cart_id);
            // $cart->status = 2;
            // $cart->save();
            if (Auth::check()) {
                return [
                    'message'   => 200,
                    'url'       => route('client.list-order-user', Auth::user()->id),
                ];
            } else {
                return [
                    'message'   => 200,
                    'url'       => route('client.list-cart', csrf_token()),

                ];
            }
        } catch (Exception $e) {
            return [
                'message'   => 500,
            ];
        }
    }
    public function FunctionName(Request $request)
    {
        # code...
        try {
            $product_detail = Product_detail::where('color',$request->color_id)->where('size',$request->size_id)->where('product_id',$request->product_id)->first();
            // return $product_detail;

            $this->order->name = $request->name;
            $this->order->phone = $request->phone;
            $this->order->address = $request->address;
            $this->order->user_id = isset(Auth::user()->id) ? Auth::user()->id : 0;
            $this->order->email = isset(Auth::user()->email) ? Auth::user()->email : $request->_token;
            $this->order->province_id = $request->province_id;
            $this->order->district_id = $request->district_id;
            $this->order->status = 0;
            $this->order->is_active = 1;
            $this->order->type_buy = 1;
            $this->order->total_money =  $request->money;
            $this->order->save();
            $orderId = $this->order->id;
            $this->order_detail->order_id = $orderId;
            $this->order_detail->total = $request->total;
            $this->order_detail->product_detail_id = $product_detail->id;
            $this->order_detail->money = $request->money;
            $this->order_detail->save();
            $cart = $this->cart->find($request->cart_id);
            $cart->status = 2;
            $cart->save();
            if (Auth::check()) {
                return [
                    'message'   => 200,
                    'url'       => route('client.list-order-user', Auth::user()->id),
                ];
            } else {
                return [
                    'message'   => 200,
                    'url'       => route('client.list-cart', csrf_token()),

                ];
            }
        } catch (Exception $e) {
            return [
                'message'   => 500,
            ];
        }
    }
    public function listOrderByUser($user_id)
    {
        $listOrderUser = $this->order->where('user_id', $user_id)
            ->where('is_active',1)
            ->orderby('id','desc')
            ->get();
        $listOrder = [];
        foreach ($listOrderUser as $order) {
            // $orderDetail    = $this->order_detail->where('order_id', $order->id)->first();
            $orderDetail    = $this->order_detail->where('order_id', $order->id)->get();
            // $productDetail      = $this->productDetail->where('id', $orderDetail->product_detail_id)->first();
            // $product            = Product::where('id', $productDetail->product_id)->first();

            $listOrder[]      = [
                'order'         => json_decode(json_encode($order), true),
                'orderDetail' =>  json_decode(json_encode($orderDetail), true),
                // 'productDetail' =>  json_decode(json_encode($productDetail), true),
                // 'product'       =>  json_decode(json_encode($product), true),
            ];
        }
        // dd(($listOrder));
        return view('client.list-order-user', compact('listOrder'));
    }
    public function editOrderInfo($order_id, Request $request)
    {
        $orderInfo = $this->order->where('id', $order_id)->first();
        $districts   = District::where('province_id', $orderInfo->province_id)->get();
        // return $districts;
        return view('client.update-info-order', compact('orderInfo', 'districts'));
    }
    public function updateOrderInfo($order_id, Request $request)
    {
        # code...
        try {
            $orderUpdate = $this->order->find($order_id);
            $orderUpdate->name = $request->name;
            $orderUpdate->phone = $request->phone;
            $orderUpdate->address = $request->address;
            $orderUpdate->province_id = $request->province_id;
            $orderUpdate->district_id = $request->district_id;
            // return $request->name;

            $orderUpdate->save();
            return [
                'message'   => 200,
                'order_id'   => $order_id,
                'url'   => route('client.list-order-user', Auth::user()->id),
            ];
        } catch (Exception $e) {
            return [
                'message'   => 500,
                'order_id'   => $order_id,
            ];
        }
    }
    public function listCart($user_id)
    {
        # code...
        // $user_id = Auth::user()->id;

        $listCartUser = Cart::where('user_id', $user_id)->where('status', 1)->get();
        // dd($listCartUser);

        $cartProduct = [];
        foreach ($listCartUser as $cart) {
            $productCartUser    = Product_detail::where('id', $cart->product_detail_id)->first();
            $product            = Product::where('id', $productCartUser->product_id)->first();
            $cartProduct[]      = [
                'cart'          => json_decode(json_encode($cart), true),
                'productDetail' =>  json_decode(json_encode($productCartUser), true),
                'product'       =>  json_decode(json_encode($product), true),
            ];
        }
        // dd($cartProduct);
        return view('client.list-cart', compact('cartProduct'));
    }
    public function deleteCart(Request $request, $cart_id)
    {
        # code...
        try {
            $cart = $this->cart->where('id', $cart_id)->first();
            $cart->status = 0;
            $cart->save();
            return 200;
        } catch (Exception $e) {
            return 500;
        }
    }

    public function paymentCart($cart_id)
    {
        # code...
        $cartDetail = $this->cart->where('id', $cart_id)->first();
        $productCart = $this->productDetail->where('id', $cartDetail->product_detail_id)->first();
        $product = $this->product->where('id', $productCart->product_id)->first();
        $color_product_id = $this->productDetail->select('color')->where('product_id', $productCart->product_id)->distinct()->get();
        $product_size = $this->productDetail->select('size')->where('product_id', $productCart->product_id)->distinct()->orderby('size', 'asc')->get();
        $colors =  Color::get();
        foreach ($colors as $color) {
            if ($color->id == $productCart->color) {
                $color_cart = $color;
            }
        }
        if ($product->is_sale == 1) {
            $money  = $cartDetail->total * $product->price * (100 - $product->percent_sale) / 100;
        } else {
            $money  = $cartDetail->total * $product->price * $cartDetail->total;
        }
        $editCart = [
            'cart'  => $cartDetail,
            'productCart'  => $productCart,
            'product'  => $product,
            'color_cart'  => $color_cart,
            'money'  => $money,
            'color_product_id'  => $color_product_id,
            'product_size'  => $product_size,
        ];
        // return $product_size;

        return view('client.edit-cart', compact('editCart'));
    }
    public function updateCart($cart_id){
        $cartDetail = $this->cart->where('id', $cart_id)->first();
        $productCart = $this->productDetail->where('id', $cartDetail->product_detail_id)->first();
        $product = $this->product->where('id', $productCart->product_id)->first();
        $color_product_id = $this->productDetail->select('color')->where('product_id', $productCart->product_id)->distinct()->get();
        $product_size = $this->productDetail->select('size')->where('product_id', $productCart->product_id)->distinct()->orderby('size', 'asc')->get();
        $colors =  Color::get();
        foreach ($colors as $color) {
            if ($color->id == $productCart->color) {
                $color_cart = $color;
            }
        }
        if ($product->is_sale == 1) {
            $money  = $cartDetail->total * $product->price * (100 - $product->percent_sale) / 100;
        } else {
            $money  = $cartDetail->total * $product->price * $cartDetail->total;
        }
        $editCart = [
            'cart'  => $cartDetail,
            'productCart'  => $productCart,
            'product'  => $product,
            'color_cart'  => $color_cart,
            'money'  => $money,
            'color_product_id'  => $color_product_id,
            'product_size'  => $product_size,
        ];
        return view('client.update-cart',compact('editCart'));
    }
    public function paymentCartList(Request $request)
    {
        # code...
        $listCartId = $request->listCart;
        $listCart =[];
        foreach($listCartId as $cart){
            $cartDetail = $this->cart->where('id', $cart)->first();
            $productCart = $this->productDetail->where('id', $cartDetail->product_detail_id)->first();
            $product = $this->product->where('id', $productCart->product_id)->first();
            $color_product_id = $this->productDetail->select('color')->where('product_id', $productCart->product_id)->distinct()->get();
            $product_size = $this->productDetail->select('size')->where('product_id', $productCart->product_id)->distinct()->orderby('size', 'asc')->get();
            $colors =  Color::get();
            foreach ($colors as $color) {
                if ($color->id == $productCart->color) {
                    $color_cart = $color;
                }
            }
            if ($product->is_sale == 1) {
                $money  = $cartDetail->total * $product->price * (100 - $product->percent_sale) / 100;
            } else {
                $money  = $cartDetail->total * $product->price * $cartDetail->total;
            }
            $listCart[] = [
                'cart'  => $cartDetail,
                'productCart'  => $productCart,
                'product'  => $product,
                'color_cart'  => $color_cart,
                'money'  => $money,
                'color_product_id'  => $color_product_id,
                'product_size'  => $product_size,
            ];
        }
        // return $listCart;
        return view('client.edit-many-cart', compact('listCart'));

        return $listCart;
    }
    public function editColor($color_id, Request $request)
    {
        # code...
        $listSizeByColorId = $this->productDetail->where('color', $color_id)->where('product_id', $request->product_id)->get();
        $product_size = $this->productDetail->select('size')->where('product_id', $request->product_id)->distinct()->orderby('size', 'asc')->get();
        $productCart = $this->productDetail->where('product_id', $request->product_id)->first();
        $product        = $this->product->where('id', $productCart->product_id)->first();

        return view('client.change-size-by-color-cart', compact('listSizeByColorId', 'product_size', 'productCart', 'product'));
    }

    public function search(Request $request)
    {
        # code...
        $listProduct = $this->product->where('name', 'like', '%' . $request->keySearch . '%')->where('is_active', 1)->get();
        return view('client.list-search', compact('listProduct'));
    }
    public function searchPage(Request $request)
    {
        # code...
        $searchKey = $request->keySearch;
        $listProduct = $this->product->where('name', 'like', '%' . $request->keySearch . '%')->where('is_active', 1)->get();

        return view('client.search-page', compact('listProduct','searchKey'));
    }

    public function getListCate($cate_id)
    {
        # code...
        $listProduct = $this->product->where('category_id', $cate_id)->where('is_active', 1)->get();
        return view('client.list-product-by-cate', compact('listProduct'));
    }
    public function getListSale()
    {
        # code...
        $listProduct = $this->product->where('is_sale', 1)->where('is_active', 1)->get();
        return view('client.list-product-by-cate', compact('listProduct'));
    }
    public function addToCart($product_id, Request $request)
    {
        # code...
        try {
            // $product = $this->productDetail->where('p',$product_id)->first();
            $product_detail_id = $this->productDetail->where('product_id', $product_id)->first();
            $this->cart->product_detail_id = $product_detail_id->id;
            $this->cart->total = 1;
            $this->cart->status = 1;
            $this->cart->user_id = isset(Auth::user()->id) ? Auth::user()->id : csrf_token();
            $this->cart->save();
            $cart = $this->cart->id;
            return [
                'message' => 200,
                'url' => route('client.home'),
            ];
        } catch (Exception $e) {
            return [
                'message' => 500,
            ];
        }
        // return $request->all();
    }

    public function listNew()
    {
        # code...
        $listNew = Post::where('is_active', 1)->get();
        return view('client.list-new', compact('listNew'));
    }
    public function detailNew($new_id)
    {
        # code...
        $detailNew = Post::where('is_active', 1)->where('id',$new_id)->first();
        return view('client.detail-new', compact('detailNew'));
    }
}
