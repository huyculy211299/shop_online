<?php

namespace App\Http\Controllers\Client;

use App\Entity\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail as SendEmail;

class AccountController extends Controller
{
    //
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function addAccount(Request $request)
    {
        $data = $request->all();
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'address' => 'required',
        ],[
            'password.confirmed' =>'Xác nhận mật khẩu không khớp',
            'email.unique' =>'Email đã được sử dụng',
        ])->validate();
        // dd($data);
        $this->user->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => 5,
            "phone" => $data['phone'],
            "address" => $data['address'],
            "province_id" => $data['province_id'],
            "district_id" => $data['district_id'],
            'password' => Hash::make($data['password']),
        ]);
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->route('client.home');
            return redirect()->intended('dashboard');
        }
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    public function getDistrictByProvinceId($province_id)
    {
        // $Districts = District::where('province_id',$province_id)->get();
        // return 1;
        // return view('client.getDistrictByProvinceId',compact('Districts'));
        if ($province_id == 0) {
            echo '<option  value=""> -- Tất cả các quận/huyện --</option>';
        }
        $districts = District::where('province_id', $province_id)->get();
        foreach ($districts as $id => $district) {
            if ($id == 0) {
                echo '<option value=""> Tất cả các quận/huyện</option>';
            }
            echo '<option value=" ' . $district->district_id . '">' . $district->district_name . '</option>';
        }
    }
    public function SendMail($subject, $email, $message) 
    {
        Mail::to($email)->send(new SendEmail($subject, $message));
    }
    public function ForgotPassword(Request $request)
    {
        // return 3;
        $mail = $request->get('email');
        if ($mail == null) {
            $request->session()->flash('error', 'Vui lòng nhập email!');
            return redirect()->back();
            // return $this->GetdataOutput(0, 500, 'Vui lòng nhập mail', '');
        }
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($chars);
        $str = "";
        for ($i = 0; $i < 6; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        $test = User::where([['email', $mail], ['is_active', 1]])->first();
        if ($test != null) {
            $update = User::where([['email', $mail], ['is_active', 1]])
                ->update([
                    'code' => $str
                ]);
            $messages = "Mã OTP xác nhận của bạn là: " . $str;
            $subject = "Shop Online nhóm 7 CNPM thông báo";
            $this->SendMail($subject, $mail, $messages);
            $request->session()->flash('success', 'Mã OTP đã được gửi qua email của bạn thành công!');
            $url = redirect()->route('client.index_check_otp', $mail)->getTargetUrl();
            return redirect($url);
            // return $this->GetdataOutput(1, 200, 'Đường link đổi mật khẩu đã được gửi tới email của bạn', null);
        } else {
            $request->session()->flash('error', 'Email tài khoản không tồn tại trên hệ thống!');
            return redirect()->back();
            // return $this->GetdataOutput(0, 500, 'Email không tồn tại', '');
        }
    }
    public function CheckOTP(Request $request)
    {
        if ($request->input('otp') == null) {
            $request->session()->flash('error', 'Vui lòng nhập mã OTP!');
            return redirect()->back();
        }
        $email = $request->input('email');
        $check = User::where([['email', $email], ['code', $request->input('otp')]])->first();
        if ($check != null) {
            $url = redirect()->route('client.index_change_password', $email)->getTargetUrl();
            return redirect($url);
        } else {
            $request->session()->flash('error', 'Mã OTP bạn nhập không đúng!');
            return redirect()->back();
        }
    }
    public function index_check_otp($mail)
    {
        return view('client.check_otp', compact('mail'));
    }
    public function index_change_password($mail)
    {
        return view('client.change_password', compact('mail'));
    }
    public function ChangePasswordWithOTP(Request $request)
    {
        $this->validate(
            $request,
            [
                'password' => 'required|min:6',
                'new_password' => 'required',
            ],
            [
                'password.required' => 'Vui lòng nhập mật khẩu mới',
                'password.min' => 'Vui lòng nhập mật khẩu mới lớn hơn 6 ký tự',
                'new_password.required' => 'Vui lòng nhập lại mật khẩu mới',
            ]
        );
        if ($request->input('password') != $request->input('new_password')) {
            $request->session()->flash('error', 'Vui lòng nhập lại đúng mật khẩu mới!');
            return redirect()->back();
        }
        $update = User::where('email', $request->input('email'))->update([
            'password'  => Hash::make($request->input('password'))
        ]);
        $request->session()->flash('success', 'Đổi mật khẩu thành công!');
        return redirect()->back();
    }
    // public function addAccount(Request $request)
    // {
    //     # code...

    // }
}
