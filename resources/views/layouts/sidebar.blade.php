  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link" style="height: 70px">
      {{-- <img src="{{asset('AdminLTE-3.0.5/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> --}}
           <img src="{{asset('/upload/logo1.jpg')}}" alt="AdminLTE Logo" class="brand-image  elevation-3" style="width: 60%;
           margin-left: 40px;
           margin-top: 2px;">
      {{-- <span class="brand-text font-weight-light">Admin</span> --}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('AdminLTE-3.0.5/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{  Auth::user()->name}}</a>
        </div>
      </div>
      <?php
        $routeName = Route::currentRouteName();
      ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if (Auth::user()->role == 1)
          <li class="nav-item">
            <a href="{{ route('home') }}" class="@if ($routeName == 'home')nav-link active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
                Tổng quan
            </a>
          </li>
            <li class="nav-item">
              <a href="{{ route('order.index') }}" class="@if ($routeName == 'order.index')nav-link active @endif">
                <i class="nav-icon fas fa-cart-plus"></i>
                  Quản lý đơn hàng
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('product.index') }}" class="@if ($routeName == 'product.index')nav-link active @endif">
                <i class="nav-icon fas fa-archive"></i>
                  Quản lý kho
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('post.index') }}" class="@if ($routeName == 'post.index')nav-link active @endif">
                <i class="nav-icon fas fa-th"></i>
                  Quản lý bài viết
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('user.index') }}" class="@if ($routeName == 'user.index')nav-link active @endif">
                <i class="nav-icon fas fa-user"></i>
                  Quản lý tài khoản
              </a>
            </li>
          @endif
          @if (Auth::user()->role == 3)
            <li class="nav-item">
              <a href="{{ route('product.index') }}" class="@if ($routeName == 'product.index')nav-link active @endif">
                <i class="nav-icon fas fa-th"></i>
                  Quản lý kho
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('product.create') }}" class="@if ($routeName == 'product.create')nav-link active @endif">
                <i class="nav-icon far fa-circle"></i>
                  Thêm sản phẩm
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('category.index') }}" class="@if ($routeName == 'category.index')nav-link active @endif">
                <i class="nav-icon fas fa-tree"></i>
                  QL Danh mục SP
              </a>
            </li>
          @endif
          @if (Auth::user()->role == 2)
            <li class="nav-item">
              <a href="{{ route('order.index') }}" class="@if ($routeName == 'order.index')nav-link active @endif">
                <i class="nav-icon fas fa-th"></i>
                  Quản lý đơn hàng
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('order.create') }}" class="@if ($routeName == 'order.create')nav-link active @endif">
                <i class="nav-icon far fa-circle"></i>
                  Thêm mới đơn hàng
              </a>
            </li>
          @endif
          @if (Auth::user()->role == 4)
            <li class="nav-item">
              <a href="{{ route('post.index') }}" class="@if ($routeName == 'post.index')nav-link active @endif">
                <i class="nav-icon fas fa-th"></i>
                  Quản lý bài viết
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('post.create') }}" class="@if ($routeName == 'post.create')nav-link active @endif">
                <i class="nav-icon far fa-circle"></i>
                  Thêm mới bài viết
              </a>
            </li>
          @endif
          {{-- @if (session('user')['role'] == 3)
                <li>
                    <a href="{{ route('account.index') }}" class="@if (
                        $routeName == 'account.index'
                        || $routeName == 'account.search'
                        || $routeName == 'account.create'
                        || $routeName == 'account.edit'
                        ) mm-active @endif font-menu">
                        <i class="fa fa-user"></i>
                        Quản lý kho
                    </a>
                </li>
          @endif --}}
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
