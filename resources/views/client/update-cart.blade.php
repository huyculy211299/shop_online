<div class="row">
    <div class="col-3">
        Chọn màu:
    </div>
    <div class="col-9 text-right changeColorId">
        @foreach ($editCart['color_product_id'] as $color)
        <a href="javascript:;" title="{{$editCart['product']->id}}" id="{{$color['color']}}"
            style="background: <?php echo \App\Entity\Color::getColorById($color['color'])->code ?>" onclick="choseEditColor(this.id,this.title)"
            data-value="" class="attr-value-cart value-attribute <?php if($color['color'] == $editCart['productCart']->color) echo 'selected-attr-value' ?>" >
        </a>
        @endforeach

    </div>
</div>
<div class="row">
    <div class="col-3">
        Chọn size:
    </div>
    <div class="col-9 text-right changeSizeId" id="product_detail_id" cart-id="{{$editCart['cart']->id}}">
        @foreach ($editCart['product_size'] as $size)
        <a href="javascript:;" title="{{$editCart['product']->id}}" id="{{$size['size']}}"
             onclick="choseEditSize(this.id)" product-detail-id="{{$editCart['productCart']->id}}"  data-product-detail-id="{{$editCart['productCart']->id}}"
            data-value="" class="attr-value-cart value-attribute <?php if($size['size'] == $editCart['productCart']->size) echo 'selected-attr-value' ?>" >
            {{$size['size']}}
        </a>
        @endforeach
    </div>
</div>
<div class="row">
    <div class="col-3">
        Số lượng:
    </div>
    <div class="col-9 text-right">
        <input type="number" name="total" value="{{$editCart['cart']->total}}" title="{{$editCart['product']->percent_sale}}"
         onchange="editTotal(this.value,this.id,this.title)" id="{{$editCart['product']->price}}" min="1" max="{{$editCart['productCart']->total_product}}" >

    </div>
</div>
<div class="row">
    <div class="col-3">
        Thành tiền:
    </div>
    <div class="col-9 text-right" id="changePriceOrder" price="{{$editCart['money']}}">
        {{number_format($editCart['money'])}} đ
    </div>
</div>
