@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
      <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
      <a href="/ao-phong-ao-thun-nam.html" class="item">Giỏ hàng</a>
    </div>
  </div>
  <div class="container">
      <div class="row">
            <div class="col-8">
                <div id="list-cart">
                    <div class="row">
                        <div class="col-12 text-right">
                            {{-- <button class="btn btn-success deleteAll">Delete All</button> --}}
                            <button class="btn btn-success paymentAll">Payment All</button>
                        </div>
                    </div>
                    @foreach ($cartProduct as $product)
                    <div class="row">
                        <div class="col-2">
                            <img src="{{asset($product['productDetail']['image'])}}" alt="Ản mô tả">
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-3">Tên sản phẩm</div>
                                <div class="col-9 text-right"> <b>{{$product['product']['name']}}</b></div>
                            </div>

                            <div class="w-100">
                                <div class="row">
                                    <div class="col-3">Màu:</div>
                                    <div class="col-9 text-right value float-right" id="attr-color">
                                        @php
                                            $color = \App\Entity\Color::getColorById($product['productDetail']['color']);
                                        @endphp
                                        <a href="javascript:;" title=""  data-color="" style="background: <?php echo $color->code ?>" data-value="" class="attr-value-cart value-attribute" >
                                        </a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="name col-3">Size:</div>
                                    <div class="col-9 text-right value float-right" id="attr-size">
                                        <a href="javascript:;" title="" data-price="" data-size="{{$product['productDetail']['size']}}" data-value="S" class="attr-value-cart value-attribute" >{{$product['productDetail']['size']}}</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="name col-3">Số lượng:</div>
                                    <div class="col-9 text-right value float-right" style="padding-right:16px" id="attr-total">
                                        {{$product['cart']['total']}}
                                        {{-- <input type="number" name="total" value="{{$product['cart']['total']}}" min="1"  id="" style="width: 40px"> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 text-right">
                            <div class="form-check">
                                <input type="checkbox" cart-id=" {{$product['cart']['id']}}" class="form-check-input" class="checkCart">
                                <label class="form-check-label" for="checkCart"></label>
                            </div>
                            <div class="w-100 mt10 text-center">
                                <span style="color:red;font-weight:bold" class="price btn-cart " price-id=" {{$product['product']['price']}}">
                                    @if ($product['product']['is_sale'] == 1)
                                        {{-- <b class="line-through"> {{number_format($product['product']['price'])}} đ</b> --}}
                                        <b style="color: red">{{number_format($product['cart']['total'] * $product['product']['price'] * (100-$product['product']['percent_sale'])/100)}} đ</b>
                                    @else
                                        <b>  {{number_format($product['cart']['total'] * $product['product']['price'])}} đ</b>
                                    @endif
                                </span>
                            </div>
                            <div class="w-100 mt10 text-center">
                                <span style="cursor: pointer;font-weight:bold" class="updateCart btn-cart bgOrange" cart-id="{{$product['cart']['id']}}">Update</span>
                            </div>
                            <div class="w-100 mt10 text-center">
                                <span style="cursor: pointer;font-weight:bold" class="deleteCart btn-cart bgRed" cart-id=" {{$product['cart']['id']}}">Delete</span>
                            </div>
                            <div class="w-100 mt10 text-center">
                                <span style="cursor: pointer;font-weight:bold" class="paymentCart btn-cart bgSuccess" cart-id=" {{$product['cart']['id']}}">Payment</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-4">
                @include('client.right-content')
            </div>
      </div>

  </div>

  <div class="modal fade" id="editFormCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title tex-center" id="exampleModalLabel">Cập nhật thông tin đơn hàng</h5>
          <button type="button" class="close" style="outline: none" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary confirmPayment">Confirm Payment</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="updateFormCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title tex-center" id="exampleModalLabel">Cập nhật thông tin đơn hàng</h5>
          <button type="button" class="close" style="outline: none" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary confirmUpdateCart">Confirm Update</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('after-scripts')
    <script>
        $('.deleteCart').click(function(){
            let cartId = $(this).attr('cart-id');
            var objectCart = $(this).parent().parent().parent();
            // alert(cartId);
            $.ajax({
                    type: 'post',
                    url: '/deleteCart/'+cartId,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        cartId: cartId,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    if(data == 200){
                        objectCart.hide();
                        alert('xoa don thanh cong');
                    }else{
                        alert('xoa don that bai');
                    }


                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        });
        $('.confirmUpdateCart').click(function(){
            let cartId = $('#product_detail_id').attr('cart-id');
            let product_detail_id = $('.changeSizeId .selected-attr-value').attr('product-detail-id');
            let total = $('input[name~="total"]').val();

            console.log(cartId);
            console.log(product_detail_id);
            console.log(total);
            $.ajax({
                    type: 'post',
                    url: '/update-cart-store/'+cartId,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        cartId: cartId,
                        product_detail_id: product_detail_id,
                        total: total,
                },
                cached:false,
                success: function(data){
                    console.log(data);

                    if(data.message == 200){
                            alert('Cập nhật giỏ hàng thành công!');
                            location.reload();

                        }else{
                            alert('Lên đơn hàng thất bại!')
                        }
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        });
        $('.updateCart').click(function(){
            let cartId = $(this).attr('cart-id');

            $.ajax({
                    type: 'get',
                    url: '/update-cart/'+cartId,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        cartId: cartId,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    $('#updateFormCart .modal-body').html(data);
                    $('#updateFormCart').modal('show');
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        });
        $('.paymentCart').click(function(){
            let cartId = $(this).attr('cart-id');

            $.ajax({
                    type: 'get',
                    url: '/payment-cart/'+cartId,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        cartId: cartId,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    $('#editFormCart .modal-body').html(data);
                    $('#editFormCart').modal('show');
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        });
        function choseEditColor(color_id,product_id){
            console.log(color_id);
            $('.changeColorId a').removeClass('selected-attr-value');
            $('#'+color_id).addClass('selected-attr-value');
            $.ajax({
                    type: 'get',
                    url: '/edit-color/'+color_id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        product_id: product_id,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    $('.changeSizeId').html(data);
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        }
        function choseEditSize(size_id){
            console.log(size_id);
            $('.changeSizeId a').removeClass('selected-attr-value');
            $('#'+size_id).addClass('selected-attr-value');
        }
        function editTotal(total,price,percent_sale){
            let money = total * price *(100- percent_sale)/100;
            let showMoney = new Intl.NumberFormat().format(money) + ' đ';
            $('#changePriceOrder').attr('price',money);
            $('#changePriceOrder').text(showMoney);
        }
        $(document).on('click','.confirmPayment',function(){
            var listCartId = [];
            $('.cart-detail-id').each(function() {
                // console.log($(this).attr('cart-id'));
                let cart_id = $(this).attr('cart-id');
                listCartId.push(cart_id);
            });
            let name = $('#nameUser').val();
            let phone = $('input[name~="phone"]').val();
            let payment_type = $('input[name~="paymentType"]').val();
            let address = $('input[name~="address"]').val();
            let province_id = $('select[name~="province_id"]').val();
            let district_id = $('select[name~="district_id"]').val();
            let color_id = $('.changeColorId .selected-attr-value').attr('id');
            let size_id = $('.changeSizeId .selected-attr-value').attr('id');
            let money = $('#changePriceOrder').attr('price');
            let total = $('input[name~="total"]').val();
            console.log(listCartId);

            let product_id = $('.changeColorId .selected-attr-value').attr('title');

            if(name == ''){
                $('#nameUser').css('border','1px solid red');
                $('#nameUser').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(phone == ''){
                $('input[name~="phone"]').css('border','1px solid red');
                $('input[name~="phone"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(address == ''){
                $('input[name~="address"]').css('border','1px solid red');
                $('input[name~="address"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(province_id == ''){
                $('select[name~="province_id"]').css('border','1px solid red');
                $('select[name~="province_id"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(district_id == ''){
                $('select[name~="district_id"]').css('border','1px solid red');
                $('select[name~="district_id"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(phone != '' && address != '' && name != '' && province_id != '' && district_id != ''){
                $.ajax({
                        type: 'post',
                        url: '/addOrderUser',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            name: name,
                            phone: phone,
                            address: address,
                            province_id: province_id,
                            district_id: district_id,
                            size_id: size_id,
                            color_id: color_id,
                            money: money,
                            total: total,
                            product_id: product_id,
                            listCartId: listCartId,
                    },
                    cached:false,
                    success: function(data){
                        console.log(data);
                        if(data.message == 200){
                            alert('Lên đơn hàng thành công!');
                            location.href = data.url;

                        }else{
                            alert('Lên đơn hàng thất bại!')
                        }


                    },
                    error:function(jqXHR,status,error){

                    },
                    complete:function(){

                    }
                });
            }
        });
        $(document).on('keydown','.form-group input',function(){
            if($(this).val() != ''){
                $(this).css('border','1px solid #ced4da');
                $(this).next().hide();
            }else{
                $(this).css('border','1px solid red');
                $(this).next().show();
            }

        });
        $(document).on('change','.form-group select',function(){
            if($(this).val() != ''){
                $(this).css('border','1px solid #ced4da');
                $(this).next().hide();
            }else{
                $(this).css('border','1px solid red');
                $(this).next().show();
            }

        });
        $(document).on('click','.paymentAll',function(){
            var result = confirm("Bạn có muốn đặt những đơn đã chọn?");
            if (result) {
                //Logic to delete the item
            }
            var listCart = [];
            $('.form-check input:checked').each(function() {
                // console.log($(this).attr('cart-id'));
                let cart_id = $(this).attr('cart-id');
                listCart.push(cart_id);
            });
            $.ajax({
                    type: 'get',
                    url: '/payment-cart-list',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        listCart: listCart,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    $('#editFormCart .modal-body').html(data);
                    $('#editFormCart').modal('show');
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        });
    </script>
@endsection
