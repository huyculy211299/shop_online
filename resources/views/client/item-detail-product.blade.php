<div class="product-detail mt-2">
    <div class="row">
        <div class="col-5">
            <h4 class="text-danger">Mô tả chi tiết</h4>
        </div>
        {{-- <div class="col-7">
            <a href="{{url('cart',$productDetail['id'])}}" product-id="{{$productDetail['id']}}" class="postion-absolute position-cart">
                <i class="fa fa-cart-plus"></i>
            </a>
            <a href="{{url('order',$productDetail['id'])}}" class="postion-absolute position-buy">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </a>
        </div> --}}
    </div>

    <p><strong>{{$productDetail['name']}}</strong></p>
    <p style="color: red">
            <strong>
            @if ($productDetail['is_sale'] == 1)
                <b class="line-through" style="color: #000"> {{number_format($productDetail['price'])}} đ</b>
                <b style="color: red">{{number_format($productDetail['price'] * (100-$productDetail['percent_sale'])/100)}} đ</b>
            @else
                <b style="color:red"> {{number_format($productDetail['price'])}} đ</b>
            @endif
            {{-- {{number_format($productDetail['price'])}} đ --}}
        </strong>
    </p>

    <p>
        {{-- {{$productDetail['description']}} --}}
        {!! $productDetail['description'] !!}

    <p>
        <div class="row">
            <div class="col-3">
                <img src="{{asset($productDetail['image'])}}" alt="">

            </div>
            @foreach ($productDetail['productSize'] as $productSize)
            <div class="col-3">
                <img src="{{asset($productSize['image'])}}" alt="">

            </div>
            @endforeach
        </div>
    </p>
</div>
