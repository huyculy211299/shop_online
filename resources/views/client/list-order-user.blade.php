@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
      <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
      <a href="/ao-phong-ao-thun-nam.html" class="item">Đơn hàng</a>
    </div>
  </div>
  <div class="container">
      <div class="row">
            <div class="col-8">
                @if (count($listOrder) > 0)
                <div id="list-cart">
                    @foreach ($listOrder as $order)
                        @foreach ($order['orderDetail'] as $orderDetail)
                            @php
                                $productDetail = \App\Entity\Order_detail::getProductDetailOrder($orderDetail['product_detail_id']);
                                $product =  \App\Entity\Product::getProductById($productDetail->product_id);
                            @endphp
                        <div class="row">
                            <div class="col-2">
                                <img src="{{asset($productDetail->image)}}" alt="Ản mô tả">
                            </div>
                            <div class="col-8">
                                <div class="row">
                                    <div class="col-3">Tên sản phẩm</div>
                                    <div class="col-9 text-right"> <b>{{$product->name}}</b></div>
                                </div>

                                <div class="w-100">
                                    <div class="row">
                                        <div class="col-3">Màu:</div>
                                        <div class="col-9 text-right value float-right" id="attr-color">
                                            @php
                                                $color = \App\Entity\Color::getColorById($productDetail->color);
                                                // dd($color);
                                            @endphp
                                            <a href="javascript:;" title=""  data-color="" style="background: <?php echo $color->code ?>" data-value="" class="attr-value-cart value-attribute" >
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="name col-3">Size:</div>
                                        <div class="col-9 text-right value float-right" id="attr-size">
                                            <a href="javascript:;"  class="attr-value-cart value-attribute" >{{$productDetail->size}}</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="name col-3">Số lượng:</div>
                                        <div class="col-9 text-right value float-right" style="padding-right:16px" id="attr-total">
                                            {{$orderDetail['total']}}
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-2 text-right">

                                <div class="w-100 mt10 text-center">
                                    {{-- <span style="color:red;font-weight:bold" class="price btn-cart " price-id=" {{$order['order']['total_money']}}">{{number_format($order['order']['total_money'])}} đ</span> --}}
                                    <span style="color:red;font-weight:bold" class="price btn-cart " price-id=" {{$order['order']['total_money']}}">{{number_format($orderDetail['money'])}} đ</span>
                                </div>
                                <div class="w-100 mt10 text-center">
                                    <span style="cursor: pointer;font-weight:bold" class="editOrderInfo btn-cart bgOrange" order-id="{{$order['order']['id']}}">Update</span>
                                </div>
                                <div class="w-100 mt10 text-center">
                                    <span style="font-weight:bold" class="editOrderInfo btn-cart bgSuccess" >
                                        @if ($order['order']['status'] == 0)
                                            Waiting
                                        @endif
                                        @if ($order['order']['status'] == 1)
                                            Transporting
                                        @endif
                                        @if ($order['order']['status'] == 2)
                                            Received
                                        @endif
                                        @if ($order['order']['status'] == 3)
                                            Rejected
                                    @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <div class="row">
                            <div class="col-10 offset-2">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="nameUser">Họ và tên </label>
                                            <div>
                                                {{$order['order']['name']}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="phoneUser">Số điện thoại </label>
                                            <div>
                                                {{$order['order']['phone']}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-10 offset-2">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="nameUser">Tỉnh/ Thành phố</label>
                                            <div>
                                                {{\App\Entity\Province::getProvinceById($order['order']['province_id'])->province_name}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="phoneUser">Quận/ Huyện </label>
                                            <div>
                                                <div>
                                                    {{\App\Entity\District::getDistrictById($order['order']['district_id'])->district_name}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-10 offset-2">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="nameUser">Địa chỉ cụ thể</label>
                                            <div>
                                                {{$order['order']['address']}}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    @endforeach
                </div>
                @else
                    <p style="color:red">Bạn chưa lên đơn hàng nào!</p>
                @endif
            </div>
            <div class="col-4">
                @include('client.right-content')
            </div>
      </div>

  </div>
  <!-- Modal -->
  <div class="modal fade" id="editInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary updateOrderInfo" >Update</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('after-scripts')
    <script>
        $('.editOrderInfo').click(function(){
            var order_id = $(this).attr('order-id')
            $.ajax({
                    type: 'get',
                    url: '/edit-order-info/'+order_id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        order_id: order_id,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    $('#editInfoModal .modal-body').html(data);
                    $('#editInfoModal').modal('show');
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        });
        $('.updateOrderInfo').click(function(){
            var order_id = $(this).attr('order-id')
            let name = $('input[name~="name"]').val();
            let phone = $('input[name~="phone"]').val();
            let address = $('input[name~="address"]').val();
            let province_id = $('select[name~="province_id"]').val();
            let district_id = $('select[name~="district_id"]').val();
            if(name == ''){
                $('input[name~="name"]').css('border','1px solid red');
                $('input[name~="name"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(phone == ''){
                $('input[name~="phone"]').css('border','1px solid red');
                $('input[name~="phone"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(address == ''){
                $('input[name~="address"]').css('border','1px solid red');
                $('input[name~="address"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(province_id == ''){
                $('select[name~="province_id"]').css('border','1px solid red');
                $('select[name~="province_id"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(district_id == ''){
                $('select[name~="district_id"]').css('border','1px solid red');
                $('select[name~="district_id"]').after('<p style="color:red">Bạn chưa nhập vào trường này</p>');
            }
            if(phone != '' && address != '' && name != '' && province_id != '' && district_id != ''){
                $.ajax({
                        type: 'post',
                        url: '/update-order-info/'+order_id,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            order_id: order_id,
                            name: name,
                            phone: phone,
                            address: address,
                            province_id: province_id,
                            district_id: district_id,
                    },
                    cached:false,
                    success: function(data){
                        console.log(data);
                        if(data.message==200){
                            alert('Cập nhật đơn hàng thành công');
                            let url = data.url;
                            console.log(url);
                            // $(location).attr('href',url)
                            location.reload();
                        }else{
                            alert('Cập nhật đơn hàng thất bại');
                        }

                    },
                    error:function(jqXHR,status,error){

                    },
                    complete:function(){

                    }
                });
            }
        });
        $(document).on('keydown','.form-group input',function(){
            if($(this).val() != ''){
                $(this).css('border','1px solid #ced4da');
                $(this).next().hide();
            }else{
                $(this).css('border','1px solid red');
                $(this).next().show();
            }

        });
        $(document).on('change','.form-group select',function(){
            if($(this).val() != ''){
                $(this).css('border','1px solid #ced4da');
                $(this).next().hide();
            }else{
                $(this).css('border','1px solid red');
                $(this).next().show();
            }

        });
    </script>

@endsection
