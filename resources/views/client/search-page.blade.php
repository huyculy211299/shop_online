@extends('client.app-client')
@section('content')
<div class="container">
    <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
    <a href="javascript:;" class="item">Danh sách sản phẩm tìm kiếm</a>&gt;&nbsp;
    <a href="javascript:;" class="item">{{$searchKey}}</a>
  </div>
@include('client.item-product')
@endsection
