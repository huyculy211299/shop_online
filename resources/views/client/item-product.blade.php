<article data-cat="pro-box-14" class="article list_home represent-shirt" style="background: url({{asset('AdminLTE-3.0.5/dist/img/background-content.jpg')}}) center center no-repeat fixed;background-size: cover;">
    <div style="background-color: #fff;">
        <div style="background-color: #fff;" class="container">
            <div class=" row" id="cat14" style="padding-top: 30px;padding-bottom: 30px;">
                @if ($listProduct->count() > 0)


                    @foreach ($listProduct as $product)
                        <div class="col-3 ">
                            <div class="item">
                                <a href="{{route('client.product-detail',$product->id)}}">
                                    <div class="image p-img position-relative">
                                        <img class="lazy" src="{{asset($product->image)}}"
                                        data-original="{{asset('AdminLTE-3.0.5/dist/img/ao1.jpg')}}" alt="Áo thun kẻ ngang AT-1072"
                                        data-img-original="/media/product/250_3062__26a0332.jpg" style="display: inline;">
                                        <a href="{{url('cart',$product->id)}}" product-id="{{$product->id}}" class="postion-absolute position-cart">
                                            <i class="fa fa-cart-plus"></i>
                                        </a>
                                        <a href="{{url('order',$product->id)}}" class="postion-absolute position-buy">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <p class="name">{{$product->name}}</p>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            @if ($product->is_sale == 1)
                                                <b class="line-through"> {{number_format($product->price)}} đ</b>
                                                <b style="color: red">{{number_format($product->price * (100-$product->percent_sale)/100)}} đ</b>
                                            @else
                                                <b style="color:red"> {{number_format($product->price)}} đ</b>
                                            @endif
                                        </div>
                                    </div>

                                </a>
                            </div>
                        </div><!--item-->
                    @endforeach
                @else
                    <span style="color: red;font-weight:bold">
                        Sản phẩm chưa được cập nhật

                    </span>
                @endif
            </div>
            <div>
          </div>
        </div>
    </div>
</article>
