@foreach ($product_size as $size)
    @foreach ($listSizeByColorId as $sizeByColor)
        @if ($size['size'] == $sizeByColor['size'])
        <a href="javascript:;" title="{{$product->id}}" id="{{$size['size']}}" product-detail-id="{{$productCart->id}}"
                onclick="choseEditSize(this.id)"
            data-value="" class="attr-value-cart value-attribute <?php if($size['size'] == $productCart->size) echo 'selected-attr-value' ?>" >
            {{$size['size']}}
        </a>
        @else
        {{-- <span class="">
            {{$size['size']}}
        </span> --}}
        @endif

    @endforeach

@endforeach
