@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
      <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
      <a href="javascipt:;" class="item">Bài viết</a>&gt;&nbsp;
    </div>
  </div>
  <div class="container">
      <div class="row">
            <div class="col-8">
                <div class="product-detail mt-2">

                    <p><strong>{{$detailNew->title}}</strong></p>

                    <p>
                        {!! $detailNew->content !!}
                        {{-- {{$detailNew->content}} --}}
                    <p>
                        <div class="row">
                            <div class="col-3">
                                <img src="{{$detailNew->image}}" alt="">

                            </div>
                        </div>
                    </p>
                </div>


            </div>
            <div class="col-4">
                @include('client.right-content')
            </div>
      </div>

  </div>


@endsection
