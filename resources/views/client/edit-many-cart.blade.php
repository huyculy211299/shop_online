@foreach ($listCart as $cart)
    <div class="row">
        <div class="col-3">
            Tên sản phẩm:
        </div>
        <div class="col-9 text-right cart-detail-id"  cart-id="{{$cart['cart']->id}}" product-detail-id="{{$cart['cart']->product_detail_id}}">
            <b>{{$cart['product']->name}}</b>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            Giá:
        </div>
        <div class="col-9 text-right">
            @if ($cart['product']->is_sale == 1)
                <b>{{number_format($cart['product']->price * (100-$cart['product']->percent_sale)/100)}} đ</b>
            @else
                <b>{{number_format($cart['product']->price)}} đ</b>
            @endif

        </div>
    </div>
    <div class="row">
        <div class="col-3">
            Chọn màu:
        </div>
        <div class="col-9 text-right changeColorId">
            @foreach ($cart['color_product_id'] as $color)
            <a href="javascript:;" title="{{$cart['product']->id}}" id="{{$color['color']}}"
                style="background: <?php echo \App\Entity\Color::getColorById($color['color'])->code ?>" onclick="choseEditColor(this.id,this.title)"
                data-value="" class="attr-value-cart value-attribute <?php if($color['color'] == $cart['productCart']->color) echo 'selected-attr-value' ?>" >
            </a>
            @endforeach

        </div>
    </div>
    <div class="row">
        <div class="col-3">
            Chọn size:
        </div>
        <div class="col-9 text-right changeSizeId" id="product_detail_id">
            @foreach ($cart['product_size'] as $size)
            <a href="javascript:;" title="{{$cart['product']->id}}" id="{{$size['size']}}"
                onclick="choseEditSize(this.id)"   data-product-detail-id="{{$cart['productCart']->id}}"
                data-value="" class="attr-value-cart value-attribute <?php if($size['size'] == $cart['productCart']->size) echo 'selected-attr-value' ?>" >
                {{$size['size']}}
            </a>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            Số lượng:
        </div>
        <div class="col-9 text-right">
            <input type="number" name="total" value="{{$cart['cart']->total}}" title="{{$cart['product']->percent_sale}}"
            onchange="editTotal(this.value,this.id,this.title)" id="{{$cart['product']->price}}" min="1" max="{{$cart['productCart']->total_product}}" >

        </div>
    </div>
    <div class="row">
        <div class="col-3">
            Thành tiền:
        </div>
        <div class="col-9 text-right" id="changePriceOrder" price="{{$cart['money']}}">
            {{number_format($cart['money'])}} đ
        </div>
    </div>

@endforeach
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="nameUser">Họ và tên </label>
            <input type="text" autocomplete="off" @auth value="{{Auth::user()->name}}" @endauth class="form-control" id="nameUser" name="name"  placeholder="Nhập họ và tên">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="phoneUser">Số điện thoại </label>
            <input type="text" autocomplete="off"  @auth value="{{Auth::user()->phone}}" @endauth onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10" class="form-control" id="phoneUser" name="phone"  placeholder="Nhập số điện thoại">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="province_id">Tỉnh/ Thành phố</label>
            <select required autocomplete="province_id" name="province_id"  id="province_id" class="form-control @error('province_id') is-invalid @enderror">
                <option value="">Chọn Tỉnh/ Thành phố</option>
                @foreach (\App\Entity\Province::orderby('sort_id','asc')->get() as $province)
                    @auth
                    <option onselect="choseProvinceChangeDistrict(this.value)"  value="{{$province->province_id}}"
                        @if (Auth::user()->province_id == $province->province_id)
                            selected
                        @endif >
                        {{$province->province_name}}
                    </option>
                    @else
                    <option onselect="choseProvinceChangeDistrict(this.value)"  value="{{$province->province_id}}"   > {{$province->province_name}}</option>

                    @endauth
                @endforeach
            </select>
        </div>

    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="nameUser">Quận/ Huyện </label>
            <select required  autocomplete="district_id" name="district_id" id="district_id" class="form-control @error('district_id') is-invalid @enderror">
                @auth
                    @php
                        $district = \App\Entity\District::getDistrictById(Auth::user()->district_id)
                    @endphp
                    <option   value="{{$district->district_id}}"
                        @if (Auth::user()->district_id == $district->district_id)
                            selected
                        @endif >
                        {{$district->district_name}}
                    </option>
                @else
                    <option value="">Chọn quận/ huyện</option>
                @endauth
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="nameUser">Địa chỉ cụ thể </label>
            <input type="text" autocomplete="off"  @auth value="{{Auth::user()->address}}" @endauth class="form-control" id="nameUser" name="address"  placeholder="Nhập vào địa chỉ">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-check form-check-inline">
            <input class="form-check-input" checked type="radio" name="paymentType" id="inlineRadio2" value="1">
            <label class="form-check-label" for="inlineRadio2">Thanh toán COD</label>
        </div>
    </div>
    <div class="col-6">
        <div class="form-check form-check-inline">
            <input class="form-check-input"  type="radio" name="paymentType" id="inlineRadio3" value="2" disabled>
            <label class="form-check-label" for="inlineRadio3">Thanh toán trực tuyết</label>
        </div>
    </div>
</div>
<script>
    $('#province_id').change(function(){
            var province_id = $(this).val();
            $.get("/get-district-by-province_id/" + province_id, function (data) {

                $("#district_id").removeAttr('disabled');
                $("#district_id").html(data);
            });

        });
</script>


