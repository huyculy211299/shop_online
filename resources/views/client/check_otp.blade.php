@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
        <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
        <a href="/ao-phong-ao-thun-nam.html" class="item">Nhập mã OTP</a>
    </div>
  <div class="container">
    <div class="row justify-content-center">
        @if (session('error'))
            <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                <div class="alert alert-danger mg-b-0 " role="alert">
                    {{ session('error') }}
                    <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                </div>
            </div>
        @endif
        @if (session('success') && !session('error'))
            <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                <div class="alert alert-success mg-b-0 ">
                    {{session('success')}}
                    <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                </div>
            </div>
        @endif
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Nhập mã OTP') }}</div>

                <div class="card-body">
                    <form method="GET" action="{{ route('client.CheckOTP') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="otp" class="col-md-4 col-form-label text-md-right">{{ __('OTP') }}</label>

                            <div class="col-md-6">
                                <input id="otp" type="otp" placeholder="Nhập mã otp" class="form-control @error('otp') is-invalid @enderror" name="otp" value="{{ old('otp') }}" required autocomplete="otp">
                                <input type="text" hidden value="{{$mail}}" name="email">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Kiểm tra') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>


@endsection
@section('after-scripts')
    <script>
        $('#province_id').change(function(){
            var province_id = $(this).val();
            $.get("/get-district-by-province_id/" + province_id, function (data) {

                $("#district_id").removeAttr('disabled');
                $("#district_id").html(data);
            });

        });
    </script>
@endsection
