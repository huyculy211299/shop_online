<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="nameUser">Họ và tên </label>
            <input type="text" autocomplete="off" class="form-control" value="{{$orderInfo->name}}" id="nameUser" name="name"  placeholder="Nhập họ và tên">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="phoneUser">Số điện thoại </label>
            <input type="text" autocomplete="off"  value="{{$orderInfo->phone}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10" class="form-control" id="phoneUser" name="phone"  placeholder="Nhập số điện thoại">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="province_id">Tỉnh/ Thành phố</label>
            <select required autocomplete="province_id" name="province_id"  id="province_id" class="form-control @error('province_id') is-invalid @enderror">
                <option value="">Chọn Tỉnh/ Thành phố</option>
                @foreach (\App\Entity\Province::orderby('sort_id','asc')->get() as $province)
                    <option onselect="choseProvinceChangeDistrict(this.value)"  value="{{$province->province_id}}"
                        @if ( $orderInfo->province_id == $province->province_id) selected @endif>
                         {{$province->province_name}}
                    </option>
                @endforeach
            </select>
        </div>

    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="nameUser">Quận/ Huyện </label>
            <select required  autocomplete="district_id" name="district_id" id="district_id" class="form-control @error('district_id') is-invalid @enderror">
                @foreach ($districts as $district)
                    <option value="{{$district->district_id}}" @if($orderInfo->district_id == $district->district_id)  selected @endif>{{$district->district_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="nameUser">Địa chỉ cụ thể </label>
            <input type="text"  value="{{$orderInfo->address}}" autocomplete="off" class="form-control" id="nameUser" name="address"  placeholder="Nhập vào địa chỉ">
        </div>
    </div>
</div>
<script>
    $('#province_id').change(function(){
            var province_id = $(this).val();
            $.get("/get-district-by-province_id/" + province_id, function (data) {

                $("#district_id").removeAttr('disabled');
                $("#district_id").html(data);
            });

        });
</script>

