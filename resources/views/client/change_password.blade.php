@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
        <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
        <a href="/ao-phong-ao-thun-nam.html" class="item">Đổi mật khẩu</a>
    </div>
  <div class="container">
    <div class="row justify-content-center">
        @if (session('error'))
            <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                <div class="alert alert-danger mg-b-0 " role="alert">
                    {{ session('error') }}
                    <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                </div>
            </div>
        @endif
        @if (session('success'))
            <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                <div class="alert alert-success mg-b-0 ">
                    {{session('success')}}
                    <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                </div>
            </div>
        @endif
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Đổi mật khẩu') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('client.ChangePasswordWithOTP') }}">
                        @csrf
                        <input type="text" hidden value="{{$mail}}" name="email">
                        <div class="form-group row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <label class="">Mật khẩu mới</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="password"  placeholder="Nhập mật khẩu mới" value="" autocomplete="off"  title="Nhập mật khẩu" name="password" class="form-control" >
                                @if ($errors->has('password'))
                                <span
                                    class="help-block text-danger "><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mt-2">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <label class="">Xác nhận mật khẩu mới</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="password"  placeholder="Xác nhận mật khẩu mới" value="" name="new_password" class="form-control" >
                                @if ($errors->has('new_password'))
                                <span
                                    class="help-block text-danger "><strong>{{ $errors->first('new_password') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Đổi') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>


@endsection
@section('after-scripts')
    <script>
        $('#province_id').change(function(){
            var province_id = $(this).val();
            $.get("/get-district-by-province_id/" + province_id, function (data) {

                $("#district_id").removeAttr('disabled');
                $("#district_id").html(data);
            });

        });
    </script>
@endsection
