<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Shop Quần áo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    {{--
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}"> --}}
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/summernote/summernote-bs4.css')}}">
    <link rel="stylesheet" href="{{asset('css/carousel-v2.2.1.css')}}">
    <link rel="stylesheet" href="{{asset('css/client-style.css')}}">
    <link rel="stylesheet" href="{{asset('css/cart.css')}}">

    <!-- Google Font: Source Sans Pro -->
    {{--
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wapper">
        <header class="">
            <div class="header">
                <div class="container">
                    <div class="row">

                        <div class="logo " style="margin-right:50px">
                            <a href="/"> <img src="{{asset('/upload/logo1.jpg')}}" alt="logo H2TShop" style="width:100px"> </a>
                        </div>

                        <div class=" col-6 header-search position-relative">
                            <form action="{{route('client.search-page')}}" method="get" enctype="multipart/form-data" name="searchForm">
                                <input type="text" name="keySearch" placeholder="Tìm kiếm" id="text_search" class="form_search">
                                <button type="submit" class="submitFormSearh header-button_submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </form>
                            <div class="listProductSearch" style="display:none">
                            </div>
                        </div>

                        <div class="col-3 header-contact-cart ">
                            @if(Auth::check())
                                @php
                                    $user_id = isset(Auth::user()->id) ? Auth::user()->id : csrf_token();
                                    $totalCart = \App\Entity\Cart::getTotalCart($user_id);
                                    // dd($user_id);
                                @endphp
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if (Auth::user()->role == 5)
                                        <a class="dropdown-item" href="{{ route('client.list-order-user',Auth::user()->id) }}" >
                                            {{ __('Danh sách đơn hàng') }}
                                        </a>
                                    @endif
                                    @if (Auth::user()->role == 4)
                                        <a class="dropdown-item" href="{{ route('post.index') }}" >
                                            {{ __('Quản lí bài viết') }}
                                        </a>
                                    @endif
                                    @if (Auth::user()->role == 3)
                                        <a class="dropdown-item" href="{{ route('product.index') }}" >
                                            {{ __('Quản lí kho') }}
                                        </a>
                                    @endif
                                    @if (Auth::user()->role == 2)
                                        <a class="dropdown-item" href="{{ route('order.index') }}" >
                                            {{ __('Quản lí đơn hàng') }}
                                        </a>
                                    @endif
                                    @if (Auth::user()->role == 1)
                                        <a class="dropdown-item" href="{{ route('home') }}" >
                                            {{ __('Quản trị') }}
                                        </a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Đăng xuất') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                                <a href="{{route('client.list-cart',Auth::user()->id)}}" class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    @if ($totalCart->count() > 0)
                                    <span style="    position: absolute;
                                    top: 0px;
                                    /* right: 58px; */
                                    z-index: 100000;
                                    color: #fff;
                                    font-weight: bold;">{{ $totalCart->count()}}</span></a>
                                    @endif

                            @else
                                <a href="{{route('client.list-cart',csrf_token())}}" class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>

                                <a href="javascript:" class="action" data-toggle="modal" data-target="#loginModal"> Đăng nhập</a>
                                <a href="{{url('/dang-ky-tai-khoan')}}" class="action"> Đăng ký </a>
                            @endif

                        </div>



                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <!-- menu homepage -->
            <div id="navbar">
                <nav class="global-nav">
                    <div class="container">
                        <ul class="ul">
                            <li class="item"><a href="/" class="parant-category">TRANG CHỦ</a></li>
                            @foreach ( \App\Entity\Category::getListCateParent() as $cate)
                            <li class="item">
                                <a href="javascript:;" class="parant-category">{{$cate->name}}</a>

                                <i class="angle-down"></i>
                                <div class="sub-category">
                                    @foreach (\App\Entity\Category::getCategoryParent($cate->id) as $chilCate)
                                        <a href="{{route('client.get-list-cate',$chilCate->id)}}" class="child-category">{{$chilCate->name}}</a>
                                    @endforeach
                                </div>

                            </li>
                            @endforeach

                            <li class="item">
                                <a href="{{route('client.get-list-sale')}}" class="parant-category">SALE OFF</a>
                            </li>

                            <li class="item"><a href="{{route('client.list-new')}}" class="parant-category">TIN TỨC</a></li>
                            {{-- <li class="item"><a href="/lien-he" class="parant-category">LIÊN HỆ</a></li> --}}
                        </ul>
                    </div>
                </nav>
            </div>

        </header>
        @yield('content')
        <footer style="background: #000;color:#fff;width:100%">
            <div class="container ">
                <div class="row pt-5">
                    <div class="col-4">
                        <h5>
                            Thông tin liên hệ
                        </h5>
                        <ul>
                            <li>Cửa hàng tại Hà Nội</li>
                            <li>Tổng Đài Online: 0972218408</li>
                            <li>Địa chỉ email: shoppnhom7@gmail.com</li>
                            <li>Địa chỉ: Km 10 Nguyễn Trãi- Hà Đông- Hà Nội</li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <h5>Về chúng tôi</h5>
                        <ul>
                            <li> <a href="http://127.0.0.1:8000/detail-new/4" style="color: #fff">Giới thiệu cửa hàng</a> </li>
                            <li>Báo cáo về của hàng</li>
                            <li>Chính sách bảo mật</li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <h5>
                        Đăng ký nhận thông báo </h5>
                        <div class="form-group mt-2">
                            <input type="text" name="" class="form-control" id="">
                            <button type="submit" class="btn btn-success mt-2">Đăng ký</button>
                        </div>
                    </div>
                </div>
            </div>

        </footer>
    </div>


      <!-- Modal -->
      <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title text-center" id="exampleModalLongTitle">Đăng nhập</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            {{-- <div class="card-header">{{ __('Login') }}</div> --}}

                            <div class="card-body">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="email" class="col-md-12 col-form-label ">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-12">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-12 col-form-label ">{{ __('Password') }}</label>

                                        <div class="col-md-12">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    {{-- <div class="form-group row">
                                        <div class="col-md-6 offset-md-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember Me') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div> --}}

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6">
                                            <a href="{{url('/nhap-ten-tai-khoan')}}" >Quên mật khẩu?</a>
                                            {{-- @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif --}}
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary text-right">
                                                {{ __('Đăng nhập') }}
                                            </button>

                                            {{-- @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif --}}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> --}}
          </div>
        </div>
      </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/jquery/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/chart.js/Chart.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/sparklines/sparkline.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('AdminLTE-3.0.5/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/moment/moment.min.js')}}"></script>
    <script src="{{asset('AdminLTE-3.0.5/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('AdminLTE-3.0.5/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('AdminLTE-3.0.5/dist/js/adminlte.js')}}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('AdminLTE-3.0.5/dist/js/pages/dashboard.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('AdminLTE-3.0.5/dist/js/demo.js')}}"></script>
    <script src="{{asset('js/carousel-v2.3.4.js')}}"></script>

    @yield('after-scripts')
    <script>
        $(document).ready(function(){
            console.log($('div.listProductSearch').height());
        });
       $('.carousel').carousel();
       $(document).on('keyup','#text_search',function(e){
            let keySearch = $(this).val();
            $.ajax({
                    type: 'get',
                    url: '/search',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        keySearch: keySearch,
                },
                cached:false,
                success: function(data){
                    // console.log(data);
                    $('.listProductSearch').html(data);
                    $('.listProductSearch').show();
                    let heighListSearch = $('div.listProductSearch').height();
                    if(heighListSearch < 90){
                        $('.listProductSearch').css('height','90px');
                    }
                    if(heighListSearch < 180){
                        $('.listProductSearch').css('height','180px');
                    }
                    if(heighListSearch > 250){
                        $('.listProductSearch').css('height','250px');
                        $('.listProductSearch').css('overflow-y','auto');
                    }


                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
       })
       $(document).on('click','.position-cart',function(e){
           e.preventDefault();
           let product_id = $(this).attr('product-id');
           console.log(product_id);
           $.ajax({
                    type: 'post',
                    url: '/add-to-cart/' +product_id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        product_id: product_id,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    if(data.message == 200){
                        alert('Thêm vào giỏ hàng thành công');
                        // window.location.href = data.url;
                        // window.location.assign(data.url);
                        // location.href(data.url);
                    }else{
                        alert('Thêm vào giỏ hàng thất bại');

                    }
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
       })
    </script>

</body>

</html>
