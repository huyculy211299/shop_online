@extends('client.app-client')
@section('content')
<link href="{{asset('magiczoom-trial/magiczoom/magiczoom.css')}}" rel="stylesheet" type="text/css" media="screen" />
<script src="{{asset('magiczoom-trial/magiczoom/magiczoom.js')}}" type="text/javascript"></script>
<style>
     .selectors {
            margin-top: 10px;
        }

        .selectors .mz-thumb img {
            max-width: 56px;
        }
        .app-figure img{
            max-height: 210px!important;
        }
</style>
    <div class="breadcrumb">
        <div class="container">
        <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
        <a href="/ao-phong-ao-thun-nam.html" class="item">Lên đơn hàng</a>&gt;&nbsp;
        <a href="/ao-thun-khong-co.html" class="item">Sản phẩm </a>
        </div>
    </div>
    <div class="container">
        <div class="row">
                <div class="col-4">
                    <div class="view-img">
                        <div class="app-figure" id="zoom-fig">
                            <a id="Zoom-1" data-options="zoomMode: magnifier; variableZoom: true" class="MagicZoom" title="Show your product in stunning detail with Magic Zoom."
                            href="{{asset($productDetail['image'])}}"
                            data-zoom-image-2x="{{asset($productDetail['image'])}}"
                                data-image-2x="{{asset($productDetail['image'])}}">
                                <img src="{{asset($productDetail['image'])}}" srcset="{{asset($productDetail['image'])}}" alt="" />
                            </a>
                            <div class="selectors d-row">
                                @foreach ($productDetail['productSize'] as $product)
                                <a data-zoom-id="Zoom-1" href="{{asset($product['image'])}}"
                                data-image="{{asset($product['image'])}}"
                                    >
                                    <img src="{{asset($product['image'])}}" />
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8" id="config-holder">
                    <h1 class="h-title left" style="font-size: 21px; font-weight: 500;">{{$productDetail['name']}}</h1>
                    <div class="d-row config-color">
                        <div class="name">Giá:</div>
                        <div class="price price_config" id="attr-price-product" data-price="{{$productDetail['price']}}">
                            @if ($productDetail['is_sale'] == 1)
                                <b class="line-through"> {{number_format($productDetail['price'])}} đ</b>
                                <b style="color: red">{{number_format($productDetail['price'] * (100-$productDetail['percent_sale'])/100)}} đ</b>
                            @else
                                <b> {{number_format($productDetail['price'])}} đ</b>
                            @endif

                        </div>
                    </div>
                    <div class="d-row config-color">
                        <div class="name">Chọn màu:</div>
                        <div class="changeColorId">
                            @foreach (\App\Entity\Product_detail::getColorByProductId($productDetail['id']) as $color)
                            <a href="javascript:;"
                                title="{{$productDetail['id']}}"
                                id="{{$color['color']}}"
                                style="background: <?php echo \App\Entity\Color::getColorById($color['color'])->code ?>" onclick="choseEditColor(this.id,this.title)"
                                data-value="" class="attr-value-cart value-attribute " >
                            </a>
                            @endforeach

                        </div>
                    </div>

                    <div class="d-row config-color">
                        <div class="name">Chọn size:</div>
                        <div class="value changeSizeId" id="attr-size" style="width:350px">
                            @foreach (\App\Entity\Product_detail::getSizeByProductId($productDetail['id']) as $size)
                            <a href="javascript:;" title="" data-price="" data-size="{{$size['size']}}"  class="attr-value value-attribute" >{{$size['size']}}</a>

                            @endforeach
                        </div>
                    </div>
                    <div class="d-row config-color">
                        <div class="name">Số lượng:</div>
                        <div class="value" id="attr-total">
                            <input type="number" name="total" percent_sale="{{$productDetail['percent_sale']}}" value="1" min="1"  id="" style="width: 40px;outline:none">
                        </div>
                    </div>
                    <div class="d-row config-color">
                        <div class="name">Thành tiền:</div>
                        <div class="value" id="attr-price"  data-price="{{$productDetail['price']}}" style="color: red;font-weight:bold">
                            @if ($productDetail['is_sale'] == 1)
                            <b style="color: red">{{number_format($productDetail['price'] * (100-$productDetail['percent_sale'])/100)}} đ</b>
                        @else
                            <b> {{number_format($productDetail['price'])}} đ</b>
                        @endif
                        </div>
                    </div>
                    <div>
                        <button type="button" class="btn btn-primary" id="buyNow">
                            Mua ngay
                        </button>
                    </div>

                </div>
        </div>
        <div class="row">
            <div class="col-12">
                @include('client.item-detail-product')
            </div>
        </div>
    </div>

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prettify/188.0.0/prettify.min.js"></script>

@endsection
@section('after-scripts')
    <script>
        $('#attr-color .attr-value').on('click',function(){
            $('#attr-color .attr-value').removeClass('selected-attr-value');
            $(this).addClass('selected-attr-value');
        });
        $('#attr-size .attr-value').on('click',function(){
            $('#attr-size .attr-value').removeClass('selected-attr-value');
            $(this).addClass('selected-attr-value');
        });
        $('#attr-total input').change(function(){
            let total = $(this).val();
            let percent_sale=0;
            percent_sale = $(this).attr('percent_sale');
            let price = '{{$productDetail["price"]}}';
            let money = total * parseInt(price) * (100 -percent_sale)/100 ;
            let showMoney = new Intl.NumberFormat().format(money);
            $('#attr-price').text(showMoney +' đ')
            $('#attr-price').attr('data-price',money);
            console.log(money);
            console.log(new Intl.NumberFormat().format(money))
        });
        $(document).on('click','#buyNow',function(){
            // console.log(1);

            var price = $('#attr-price-product').attr('data-price');
            var total = $('#attr-total input').val();
            // var color = $('#attr-color .selected-attr-value').attr('data-color');
            // var size = $('#attr-size .selected-attr-value').attr('data-size');
            var productDetail_id = "{{$productDetail['id']}}";
            let color_id = $('.changeColorId .selected-attr-value').attr('id');
            let size_id = $('.changeSizeId .selected-attr-value').attr('id');
            let size = $('.changeColorId a').hasClass('selected-attr-value');
            let color = $('.changeSizeId a').hasClass('selected-attr-value');
            console.log(color);
            console.log(size);
            if(color == false){
                $('.changeColorId').after('<span style="color:red">Bạn chưa chọn màu sản phẩm</span>');
            }
            if( size == false){
                $('.changeSizeId').after('<span style="color:red">Bạn chưa chọn size sản phẩm</span>');
            }
            if(color == true && size == true)
            {
                $.ajax({
                        type: 'post',
                        url: '/addOrder/'+ productDetail_id,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            total: total,
                            price: price,
                            color_id: color_id,
                            total: total,
                            size_id: size_id,
                            productDetail_id: productDetail_id,

                        },
                        cached:false,
                        success: function(data){
                            console.log(data);
                            if(data.message == 200){
                                alert('len don thanh cong');
                                // location.href(data.url)
                                location.href = data.url;
                            }else{
                                alert('len don that bai');
                            }


                        },
                        error:function(jqXHR,status,error){

                        },
                        complete:function(){

                        }
                });
            }

        })
        function choseEditColor(color_id,product_id){
            console.log(color_id);
            $('.changeColorId a').removeClass('selected-attr-value');
            $('#'+color_id).addClass('selected-attr-value');
            $('#'+color_id).parent().next().hide();
            $('.changeSizeId').next().hide();
            $.ajax({
                    type: 'get',
                    url: '/edit-color/'+color_id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        product_id: product_id,
                },
                cached:false,
                success: function(data){
                    console.log(data);
                    $('.changeSizeId').html(data);
                },
                error:function(jqXHR,status,error){

                },
                complete:function(){

                }
            });
        }
        function choseEditSize(size_id){
            console.log(size_id);
            $('.changeSizeId a').removeClass('selected-attr-value');
            $('#'+size_id).addClass('selected-attr-value');
        }
    </script>
@endsection
