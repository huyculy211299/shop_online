@extends('client.app-client')
@section('content')
<div class="banner">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="{{asset('AdminLTE-3.0.5/dist/img/banner.jpg')}}" alt="First slide">
            <div class="carousel-caption d-none d-md-block">
                <h5 style="color:red">Welcom to clothing PTIT</h5>
                <p>...</p>
              </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="{{asset('AdminLTE-3.0.5/dist/img/banner.jpg')}}" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <h5 style="color:red">Welcom to clothing PTIT</h5>
                <p>...</p>
              </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="{{asset('AdminLTE-3.0.5/dist/img/banner.jpg')}}" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <h5 style="color:red">Welcom to clothing PTIT</h5>
                <p>...</p>
              </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</div>
@include('client.item-product')
@endsection
