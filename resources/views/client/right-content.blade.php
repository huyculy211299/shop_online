<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item text-center wid50">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sản phẩm nổi bật</a>
    </li>
    <li class="nav-item text-center wid50">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Sản phẩm sale</a>
    </li>

</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <ul class="list-unstyled">
            @foreach (\App\Entity\Product::getProductHot() as $product)
            <a href="{{route('client.product-detail',$product->id)}}">
                <li class="media">
                    <div class="col-3">
                        <img class="" src="{{asset($product->image)}}" alt="Generic placeholder image" >

                    </div>
                    <div class="col-9">
                        <div class="media-body">
                            <h5 class="mt-0 mb-1">{{$product->name}}</h5>
                            <p style="color:red">
                                {{-- {{number_format($product->price)}} đ --}}
                                @if ($product->is_sale == 1)
                                <b class="line-through" style="color: #000"> {{number_format($product->price)}} đ</b>
                                <b style="color: red">{{number_format($product->price * (100-$product->percent_sale)/100)}} đ</b>
                            @else
                                <b style="color:red"> {{number_format($product->price)}} đ</b>
                            @endif
                            </p>
                        </div>
                    </div>
                </li>
            </a>

            @endforeach

        </ul>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <ul class="list-unstyled">
            @foreach (\App\Entity\Product::getProductSale() as $product)
            <a href="{{route('client.product-detail',$product->id)}}">
                <li class="media">
                    <div class="col-3">
                        <img class="" src="{{asset($product->image)}}" alt="Generic placeholder image">
                    </div>
                    <div class="col-9">
                        <div class="media-body">
                            <h5 class="mt-0 mb-1">{{$product->name}}</h5>
                            <p style="color:red">
                                @if ($product->is_sale == 1)
                                                <b class="line-through" style="color: #000"> {{number_format($product->price)}} đ</b>
                                                <b style="color: red">{{number_format($product->price * (100-$product->percent_sale)/100)}} đ</b>
                                            @else
                                                <b style="color:red"> {{number_format($product->price)}} đ</b>
                                            @endif
                                {{-- {{number_format($product->price)}} đ --}}
                            </p>
                        </div>
                    </div>
                </li>
            </a>
            @endforeach
        </ul>
    </div>
</div>
