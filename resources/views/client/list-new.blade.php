@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
      <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
      <a href="javascript:;" class="item">Bài viết</a>
    </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach ($listNew as $new)
                <div class="col-3">
                    <a href="{{route('client.detail-new',$new->id)}}">
                        <li class="media">
                            <div class="col-3">
                                <img class="" src="{{asset($new->image)}}" alt="Generic placeholder image" >

                            </div>
                            <div class="col-9">
                                <div class="media-body">
                                    <h5 class="mt-0 mb-1">{{$new->title}}</h5>
                                </div>
                            </div>
                        </li>
                    </a>
                </div>
                @endforeach
        </div>
    </div>

</div>

@endsection

