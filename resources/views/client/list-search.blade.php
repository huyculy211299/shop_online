<ul class="list-unstyled">
    @foreach ($listProduct as $product)
    <a href="{{route('client.product-detail',$product->id)}}">
        <li class="media">
            <div class="col-3">
                <img class="" src="{{asset($product->image)}}" alt="Generic placeholder image" >

            </div>
            <div class="col-9">
                <div class="media-body">
                    <h5 class="mt-0 mb-1">{{$product->name}}</h5>
                    @if ($product->is_sale == 1)
                        <b class="line-through"> {{number_format($product->price)}} đ</b>
                        <b style="color: red">{{number_format($product->price * (100-$product->percent_sale)/100)}} đ</b>
                    @else
                        <b style="color:red"> {{number_format($product->price)}} đ</b>
                    @endif
                </div>
            </div>
        </li>
    </a>
    @endforeach
</ul>
