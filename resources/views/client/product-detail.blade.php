@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
      <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
      <a href="javascipt:;" class="item">Thông tin sản phẩm</a>&gt;&nbsp;
      <a href="javascipt:;" class="item">{{$productDetail['name']}}</a>
    </div>
  </div>
  <div class="container">
      <div class="row">
            <div class="col-8">
                <div class="col-12">
                    <a href="{{url('cart',$productDetail['id'])}}" product-id="{{$productDetail['id']}}" class="postion-absolute position-cart">
                        <i class="fa fa-cart-plus"></i>
                    </a>
                    <a href="{{url('order',$productDetail['id'])}}" class="postion-absolute position-buy">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    </a>
                </div>
                @include('client.item-detail-product')

            </div>
            <div class="col-4">
                @include('client.right-content')
            </div>
      </div>

  </div>


@endsection
