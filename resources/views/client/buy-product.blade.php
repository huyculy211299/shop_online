@extends('client.app-client')
@section('content')
<div class="breadcrumb">
    <div class="container">
      <a href="/" class="item">Trang chủ</a>&gt;&nbsp;
      <a href="javascript:;" class="item">Áo thun nam</a>&gt;&nbsp;
      <a href="javascript:;" class="item">Áo thun không cổ</a>
    </div>
  </div>
  <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="product-detail">
                    <p><strong>Áo thun NAM</strong></p>

                    <p><strong><a href="https://h2tstore.vn/ao-thun-khong-co.html"><em>&Aacute;o thun nam</em></a></strong>&nbsp;l&agrave; trang phục th&ocirc;ng dụng nhất trong thế giới thời trang v&agrave; chưa c&oacute; một phụ kiện thời trang n&agrave;o đ&aacute;nh bật được vị tr&iacute; số 1 của n&oacute;. Lu&ocirc;n l&agrave; mẫu &aacute;o cực đơn giản nhưng lại kh&ocirc;ng hề đơn giản ch&uacute;t n&agrave;o. Ph&ugrave; hợp với mọi đối tượng sử dụng. Với thiết kế đơn giản&nbsp;mặt trước bassic trơn lưng &aacute;p họa tiết in chữ l&agrave;m điểm nhấn nổi bật cho người mặc.</p>

                    <p><strong>Thiết kế &aacute;o:</strong></p>

                    <ul>
                        <li>&Aacute;o cổ tr&ograve;n, thiết kế đơn giản, dễ phối đố.</li>
                        <li>Gam m&agrave;u lạnh ph&ugrave; hợp với mọi lứa tuổi</li>
                        <li>D&aacute;ng &aacute;o basic truyền thống.</li>
                    </ul>

                    <p><strong>Đặc t&iacute;nh &aacute;o:&nbsp;</strong></p>

                    <ul>
                        <li>Tho&aacute;ng m&aacute;t thoải m&aacute;i ( cotton)</li>
                        <li>Thấm h&uacute;t mồ h&ocirc;i tốt ( cotton)</li>
                        <li>Mềm mại, th&acirc;n thiện với da ( cotton)</li>
                    </ul>

                    <p><strong>M&agrave;u sắc:&nbsp;</strong>Đen (Bc)</p>

                    <p><strong>Size hiện c&oacute;:&nbsp;</strong>S, M, L, XL</p>

                    <p><strong>Mix and Match:&nbsp;</strong></p>

                    <ul>
                        <li>Chẳng cần phối đồ g&igrave; cho phức tạp, chỉ cần&nbsp;phối hợp&nbsp;c&ugrave;ng chiếc&nbsp;<em><strong><a href="https://h2tstore.vn/quan-bo-nam.html">quần jean</a>&nbsp;</strong></em>&nbsp;v&agrave;&nbsp;<strong><a href="https://h2tstore.vn/giay-da-nam.html"><em>đ&ocirc;i gi&agrave;y thể thao</em></a></strong>&nbsp;đ&atilde; c&oacute; 1 set đồ cực kỳ năng động rồi.</li>
                        <li>Đơn giản chỉ cần 1 chiếc&nbsp;<strong><em><a href="https://h2tstore.vn/quan-ngo.html">quần short</a></em></strong>&nbsp;v&agrave; 1 đ&ocirc;i&nbsp;<em><strong><a href="https://h2tstore.vn/giay-da-nam.html">gi&agrave;y thể thao</a>&nbsp;</strong></em>đ&atilde; cho bạn một set đồ tự tin</li>
                    </ul>
                    <p>
                        <img src="{{asset('AdminLTE-3.0.5/dist/img/ao1.jpg')}}" alt="">
                        <img src="{{asset('AdminLTE-3.0.5/dist/img/ao1.jpg')}}" alt="">
                        <img src="{{asset('AdminLTE-3.0.5/dist/img/ao1.jpg')}}" alt="">
                        <img src="{{asset('AdminLTE-3.0.5/dist/img/ao1.jpg')}}" alt="">
                        <img src="{{asset('AdminLTE-3.0.5/dist/img/ao1.jpg')}}" alt="">
                        <img src="{{asset('AdminLTE-3.0.5/dist/img/ao1.jpg')}}" alt="">
                    </p>
                </div>
            </div>
            <div class="col-4">
                @include('client.right-content')
            </div>
        </div>

  </div>


@endsection
