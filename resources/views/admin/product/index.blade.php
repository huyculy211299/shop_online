@extends('layouts.app-new')
@section('title-page', 'Danh sách sản phẩm trong kho' )
@section('content')
{{-- <div class="container"> --}}
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
            <div class="main-card card">
                <div class="card-body">
                    <form action="" id="search_lucky" method="get">
                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                <input type="text" name="name" id="name" value="{{request('name')}}" placeholder="Tên sp" class="form-control">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <?php
                                        $is_sale = '';
                                        if(isset($_GET['is_sale']))
                                        {
                                            $is_sale = $_GET['is_sale'];
                                        }
                                ?>
                                <select class="form-control" id="is_sale" name="is_sale">
                                        <option value="">--TT sale--</option>
                                        <option value="1" @if(isset($is_sale) && $is_sale == 1) selected @endif>Đang sale</option>
                                        <option value="0" @if(isset($is_sale) && $is_sale == 0 && $is_sale != '') selected @endif>Chưa sale</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <?php
                                        $is_hot = '';
                                        if(isset($_GET['is_hot']))
                                        {
                                            $is_hot = $_GET['is_hot'];
                                        }
                                ?>
                                <select class="form-control" id="is_hot" name="is_hot">
                                        <option value="">--TT HOT--</option>
                                        <option value="1" @if(isset($is_hot) && $is_hot == 1) selected @endif>Đang HOT</option>
                                        <option value="0" @if(isset($is_hot) && $is_hot == 0 && $is_hot != '') selected @endif>Không HOT</option>
                                </select>
                               
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <?php
                                        $category_id = '';
                                        if(isset($_GET['category_id']))
                                        {
                                            $category_id = $_GET['category_id'];
                                        }
                                ?>
                                <select class="form-control" id="category_id" name="category_id">
                                <option value="">--Chọn loại sp--</option>
                                @foreach($category as $c)
                                        <option value="{{$c->id}}"
                                        >{{$c->name}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-md-4 col-lg-4 offset-md-1 mt-2">
                                <?php
                                    $from_date = '';
                                    if(isset($_GET['from_date']))
                                    {
                                        $from_date = trim($_GET['from_date']);
                                    }
                                ?>
                                    <input type="date" placeholder="Từ ngày" class="form-control" name="from_date" id="from_date" value="@if($from_date != ''){{$from_date}}@endif" max="{{ date("Y-m-d") }}">
                                </div>
                                <div class="col-md-4 col-lg-4 offset-2 mt-2">
                                <?php
                                    $to_date = '';
                                    if(isset($_GET['to_date']))
                                    {
                                        $to_date = trim($_GET['to_date']);
                                    }
                                ?>
                                    <input type="date" placeholder="Đến ngày" class="form-control" name="to_date" id="to_date" value="@if($to_date != ''){{$to_date}}@endif" max="{{ date("Y-m-d") }}">
                                </div>
                        </div>
                        <div class="col-md-12 col-lg-12 text-center  mt-2">
                            <button type="submit" class="btn btn-primary mr-2 search"><i class="fa fa-search"></i> Tìm kiếm</button>
                        </div>
                    </form>
                    <div class="row mt-2">
                        <?php 
                        $ls_pd = \App\Entity\Product_detail::join('product as p','p.id','product_detail.product_id')->sum('product_detail.total_product');    
                        ?>
                        <div class="col-md-6 col-lg-6">
                            <h4>Số sản phẩm: {{count($listProduct)}}</h4>
                            <h4>Số lượng chi tiết sản phẩm: {{$ls_pd}}</h4>
                        </div>
                        @if(Auth::user()->role != 1)
                        <div class="col-md-6 col-lg-6 text-right">
                            <a href="{{route('product.create')}}" class="btn btn-success text-white">
                                <i class="fa fa-plus"></i>
                                Thêm mới
                            </a>
                            <button class="btn btn-primary"><i class="fas fa-download mr-2"></i>Xuất báo cáo</button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách sản phẩm trong kho</h5>
                    <div class="table-responsive">
                        <table id="myTable" class="mb-0 table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên SP</th>
                                    <th>Ảnh</th>
                                    <th>Giá</th>
                                    <th>Loại SP</th>
                                    <th>Xuất xứ</th>
                                    <th>TT sale</th>
                                    <th>TT HOT</th>
                                    <th>Ngày tạo</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($listProduct as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{ $item['name']}}</td>
                                        <td>
                                            @if(isset($item['image']))
                                            <img src="{{asset($item['image'])}}" width="80" height="70"/>
                                            @else
                                            <span class="text_null">{{'Chưa cập nhật'}}</span>
                                            @endif
                                        </td>
                                        <td>{{ $item['price']}}</td>
                                        <td>{{ $item['category_name']}}</td>
                                        <td>{{ $item['company']}}</td>
                                        <td>
                                            @if($item['is_sale'] == 1)
                                                Đang sale
                                            @else
                                            <span class="text_null">Chưa Sale</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item['is_hot'] == 1)
                                                Đang HOT
                                            @else
                                            <span class="text_null">Chưa HOT</span>
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($item['created_at']))}}</td>
                                        <td>
                                            @if(Auth::user()->role != 1)
                                            <a href="{{route('product.update',$item['id'])}}" class="btn btn-warning"><i class="fa fa-edit pd-1"></i> </a>
                                            <a href="{{route('product.delete',$item['id'])}}" class="btn btn-danger delete_product"><i class="fa fa-trash pd-1"></i> </a>
                                            @if($item['is_hot'] == 1)
                                                <a href="{{route('product.update_hot',$item['id'])}}" class="btn btn-info tat_hot">Tắt HOT</a>
                                            @else
                                                <a href="{{route('product.update_hot',$item['id'])}}" class="btn btn-info bat_hot">Bật HOT</a>
                                            @endif
                                            @if($item['is_sale'] == 1)
                                                <a href="{{route('product.update_sale',$item['id'])}}" class="btn btn-primary tat_sale">Tắt Sale</a>
                                            @else
                                                <a href="{{route('product.update_sale',$item['id'])}}" class="btn btn-primary bat_sale">Bật Sale</a>
                                            @endif
                                            <button href="{{route('product.create_product_detail',$item['id'])}}" type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal1" onclick="return CreateProductDetail(this);">Thêm chi tiết</button>
                                            @endif
                                            <a href="{{route('product.detail',$item['id'])}}" class="btn btn-default">Chi tiết </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                
                            </tbody>
                        </table>
                        <div class="pagination_page mt-2" style="float: right">
                            <nav>
                                {{ $listProduct->appends(Request::all())->links() }}
                            </nav>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form role="form" action=""  method="POST" id="create_product_detail" enctype="multipart/form-data">
                {!! csrf_field() !!}
          <!-- Modal content-->
                <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm mới</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Hình ảnh </label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="file" required accept="image/*"
                                    onchange="preview_image(event, 'image')" 
                                    name="image">
                                <img width="100px" class="mt-2"  id="image">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Size</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập size" value="" autocomplete="off"  title="Nhập size" name="size" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Màu</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <select class="form-control select2" id="color"
                                    name="color">
                                    <option value="">--Chọn màu--</option>
                                    @foreach($color as $c)
                                            <option value="{{$c->id}}"
                                            >{{$c->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Số lượng</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập số lượng" value="" autocomplete="off"  title="Nhập số lượng" name="total_product" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary send1">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
{{-- </div> --}}
@endsection
@section('scripts')
    <script>
        function CreateProductDetail(e) {
            var url = $(e).attr('href');
            
            var Ids = [];
            console.log(url);
            $('#create_product_detail').attr('action', url);
            return false;
        }
        $('.delete_product').click(function(){
            var x = confirm("Bạn có chắc chắn muốn xóa?");
            if (x)
                return true;
            else
                return false;
        });
        $('.tat_hot').click(function(){
            var x = confirm("Bạn có chắc chắn muốn tắt HOT?");
            if (x)
                return true;
            else
                return false;
        });
        $('.bat_hot').click(function(){
            var x = confirm("Bạn có chắc chắn muốn bật HOT?");
            if (x)
                return true;
            else
                return false;
        });
        $('.tat_sale').click(function(){
            var x = confirm("Bạn có chắc chắn muốn tắt sale?");
            if (x)
                return true;
            else
                return false;
        });
        $('.bat_sale').click(function(){
            var x = confirm("Bạn có chắc chắn muốn bật sale?");
            if (x)
                return true;
            else
                return false;
        });
    </script>
    @endsection

