@extends('layouts.app-new')
@section('title-page', 'Cập nhật sản phẩm' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('product.edit',$product->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 ">
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Tên sản phẩm</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                        <input name="name" placeholder="Tên quà tặng" value="{{$product->name}}" type="text" autocomplete="off"
                                                class="form-control" required>
                                            @if ($errors->has('name'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Giá</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <input type="text"  placeholder="Nhập giá" value="{{$product->price}}" autocomplete="off"  title="Nhập giá" name="price" class="form-control"  required>
                                                @if ($errors->has('price'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('price') }}</strong></span>
                                                @endif
                                            </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Loại SP</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <select class="form-control" id="category_id"
                                                name="category_id">
                                                <option value="{{$product->category_id}}">--Chọn loại sp--</option>
                                                @foreach($category as $c)
                                                        <option value="{{$c->id}}" @if($product->category_id == $c->id ) selected @endif
                                                        >{{$c->name.' - '.$c->parent_name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Xuất xứ</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <input type="text"  placeholder="Nhập xuất xứ" value="{{$product->company}}" autocomplete="off"  title="Nhập xuất xứ" name="company" class="form-control" >
                                            @if ($errors->has('company'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('company') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Trạng thái sale</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <select class="form-control" id="is_sale" name="is_sale">
                                                <option value="0" @if($product->is_sale == 0) selected @endif>Không Sale</option>
                                                <option value="1" @if($product->is_sale == 1) selected @endif>Sale</option>
                                        </select>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Hình ảnh </label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <input type="file" accept="image/*"
                                                onchange="preview_image(event, 'image')" 
                                                name="image">
                                        <img src="{{asset($product->image)}}" width="100px" class="mt-2"  id="image">
                                        </div>
                                    </div>
                                    
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">% sale</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <input type="text"  placeholder="Nhập %" value="{{$product->percent_sale}}" autocomplete="off"  title="Nhập %" name="percent_sale" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Trạng thái HOT</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <select class="form-control" id="is_hot" name="is_hot">
                                                <option value="0" @if($product->is_hot == 0) selected @endif>Không HOT</option>
                                                <option value="1" @if($product->is_hot == 1) selected @endif>HOT</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Hiển thị lên web</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <select class="form-control" id="status" name="status">
                                                <option value="0" @if($product->status == 0) selected @endif>Không</option>
                                                <option value="1" @if($product->status == 1) selected @endif>Có</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 mt-2 mt-2">
                                    <label for="">Mô tả</label>
                                    <textarea placeholder="Nhập mô tả" rows="5" cols="80" name="description" id="ckeditor" class="form-control">{{$product->description}}</textarea>
                                    @if ($errors->has('description'))
                                        <span
                                            class="help-block text-danger "><strong>{{ $errors->first('description') }}</strong></span>
                                    @endif
                                </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                    <a href="{{route('product.index')}}" class="mt-2 btn btn-danger text-white"><i class="fa fa-arrow-circle-o-left"></i>
                                        Trở về</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
