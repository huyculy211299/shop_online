@extends('layouts.app-new')
@section('title-page', 'Chi tiết sản phẩm' )
@section('content')
{{-- <div class="container"> --}}
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
            @if(Auth::user()->role != 1)
            <div class="main-card card">
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="col-md-6 col-lg-6">
                            <button href="{{route('product.create_product_detail',$product->id)}}" type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal1" onclick="return CreateProductDetail(this);">Thêm chi tiết SP</button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách chi tiết sp</h5>
                    <div class="table-responsive">
                        <table id="myTable" class="mb-0 table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Size</th>
                                    <th>Ảnh</th>
                                    <th>Màu</th>
                                    <th>Số lượng</th>
                                    <th>Ngày tạo</th>
                                    @if(Auth::user()->role != 1)
                                    <th>Thao tác</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($product_detail as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{ $product->name}}</td>
                                        <td>{{ $item['size']}}</td>
                                        <td>
                                            @if(isset($item['image']))
                                            <img src="{{asset($item['image'])}}" width="80" height="70"/>
                                            @else
                                            <span class="text_null">{{'Chưa cập nhật'}}</span>
                                            @endif
                                        </td>
                                        <td>{{ $item['color_name']}}</td>
                                        <td>{{ $item['total_product']}}</td>
                                        <td>{{date('d-m-Y',strtotime($item['created_at']))}}</td>
                                        @if(Auth::user()->role != 1)
                                        <td>
                                            <a href="{{route('product.delete_detail',$item['id'])}}" class="btn btn-danger delete_product"><i class="fa fa-trash pd-1"></i> </a>
                                            <button href="{{route('product.update_product_detail',$item['id'])}}"
                                             image="{{$item['image']}}"  size="{{$item['size']}}" color="{{$item['color']}}" total_product="{{$item['total_product']}}"
                                             type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal2" onclick="return UpdateProductDetail(this);">Sửa</button>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form role="form" action=""  method="POST" id="create_product_detail" enctype="multipart/form-data">
                {!! csrf_field() !!}
          <!-- Modal content-->
                <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm mới</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Hình ảnh </label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="file" required accept="image/*"
                                    onchange="preview_image(event, 'image')" 
                                    name="image">
                                <img width="100px" class="mt-2"  id="image">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Size</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập size" value="" autocomplete="off"  title="Nhập size" name="size" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Màu</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <select class="form-control select2" id="color"
                                    name="color">
                                    <option value="">--Chọn màu--</option>
                                    @foreach($color as $c)
                                            <option value="{{$c->id}}"
                                            >{{$c->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Số lượng</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập số lượng" value="" autocomplete="off"  title="Nhập số lượng" name="total_product" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary send1">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
      <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form role="form" action=""  method="POST" id="update_product_detail" enctype="multipart/form-data">
                {!! csrf_field() !!}
          <!-- Modal content-->
                <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                        <h4 class="modal-title">Sửa</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Hình ảnh </label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="file" accept="image/*"
                                    onchange="preview_image(event, 'image')" 
                                    name="image">
                                <img width="100px" class="mt-2"  id="image_detail">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Size</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập size" value="" autocomplete="off"  title="Nhập size" name="size" id="size" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Màu</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <select class="form-control" id="color_update"
                                    name="color">
                                    <option value="">--Chọn màu--</option>
                                    @foreach($color as $c)
                                            <option value="{{$c->id}}" 
                                            >{{$c->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Số lượng</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập số lượng" value="" autocomplete="off"  title="Nhập số lượng" name="total_product" id="total_product"  class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary send1">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
{{-- </div> --}}
@endsection
@section('scripts')
    <script>
        function CreateProductDetail(e) {
            var url = $(e).attr('href');
            $('#create_product_detail').attr('action', url);
            return false;
        }

        function UpdateProductDetail(e) {
            var url = $(e).attr('href');
            var image = $(e).attr('image');
            var size = $(e).attr('size');
            var color = $(e).attr('color');
            var total_product = $(e).attr('total_product');
            $('#update_product_detail').attr('action', url);
            $('#image_detail').attr('src', "http://127.0.0.1:8000"+image);
            $('#size').attr('value', size);
            $('#total_product').attr('value', total_product);
            $("#color_update option[value=" + color +"]").attr('selected', 'selected'); 
            return false;
        }

        $('.delete_product').click(function(){
            var x = confirm("Bạn có chắc chắn muốn xóa?");
            if (x)
                return true;
            else
                return false;
        });
        $('.tat_hot').click(function(){
            var x = confirm("Bạn có chắc chắn muốn tắt HOT?");
            if (x)
                return true;
            else
                return false;
        });
        $('.bat_hot').click(function(){
            var x = confirm("Bạn có chắc chắn muốn bật HOT?");
            if (x)
                return true;
            else
                return false;
        });
        $('.tat_sale').click(function(){
            var x = confirm("Bạn có chắc chắn muốn tắt sale?");
            if (x)
                return true;
            else
                return false;
        });
        $('.bat_sale').click(function(){
            var x = confirm("Bạn có chắc chắn muốn bật sale?");
            if (x)
                return true;
            else
                return false;
        });
    </script>
    @endsection

