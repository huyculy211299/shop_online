@extends('layouts.app-new')
@section('title-page', 'Cập nhật tài khoản' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('ChangePasswordUser',$user->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Mật khẩu cũ</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="password"  placeholder="Nhập mật khẩu cũ" value="" autocomplete="off"  title="Nhập mật khẩu cũ" name="current_password" class="form-control" >
                                            @if ($errors->has('current_password'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('current_password') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Mật khẩu mới</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="password"  placeholder="Nhập mật khẩu mới" value="" autocomplete="off"  title="Nhập mật khẩu" name="password" class="form-control" >
                                            @if ($errors->has('password'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('password') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Xác nhận mật khẩu mới</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="password"  placeholder="Xác nhận mật khẩu mới" value="" name="new_password" class="form-control" >
                                            @if ($errors->has('new_password'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('new_password') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
