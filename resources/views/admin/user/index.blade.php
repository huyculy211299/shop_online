@extends('layouts.app-new')
@section('title-page', 'Danh sách tài khoản' )
@section('content')
{{-- <div class="container"> --}}
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
            <div class="main-card card">
                <div class="card-body">
                    <form action="" id="search_lucky" method="get">
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <input type="text" name="name" id="name" value="{{request('name')}}" placeholder="Họ tên" class="form-control">
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <input type="text" name="phone" id="phone" value="{{request('phone')}}" placeholder="Số điện thoại" class="form-control">
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <?php
                                        $role = '';
                                        if(isset($_GET['role']))
                                        {
                                            $role = $_GET['role'];
                                        }
                                ?>
                                <select class="form-control select2" id="role" name="role">
                                <option value="">--Chọn loại tài khoản--</option>
                                @foreach($ls_role as $r)
                                        <option value="{{$r->id}}" @if(isset($role) && $role == $r->id) selected @endif 
                                        >{{$r->name}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-md-4 col-lg-4 offset-md-1 mt-2">
                                <?php
                                    $from_date = '';
                                    if(isset($_GET['from_date']))
                                    {
                                        $from_date = trim($_GET['from_date']);
                                    }
                                ?>
                                    <input type="date" placeholder="Từ ngày" class="form-control" name="from_date" id="from_date" value="@if($from_date != ''){{$from_date}}@endif" max="{{ date("Y-m-d") }}">
                                </div>
                                <div class="col-md-4 col-lg-4 offset-2 mt-2">
                                <?php
                                    $to_date = '';
                                    if(isset($_GET['to_date']))
                                    {
                                        $to_date = trim($_GET['to_date']);
                                    }
                                ?>
                                    <input type="date" placeholder="Đến ngày" class="form-control" name="to_date" id="to_date" value="@if($to_date != ''){{$to_date}}@endif" max="{{ date("Y-m-d") }}">
                                </div>
                        </div>
                        <div class="col-md-6 col-lg-6 text-right mt-2">
                            <button type="submit" class="btn btn-primary mr-2 search"><i class="fa fa-search"></i> Tìm kiếm</button>
                        </div>
                    </form>
                    <div class="row mt-2">
                        <div class="col-md-6 col-lg-6">
                            <h4>Tổng số tài khoản: {{count($listUser)}}</h4>
                        </div>
                        <div class="col-md-6 col-lg-6 text-right">
                            <a href="{{route('user.create')}}" class="btn btn-success text-white">
                                <i class="fa fa-plus"></i>
                                Thêm mới
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách tài khoản</h5>
                    <div class="table-responsive">
                        <table id="myTable" class="mb-0 table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Họ tên</th>
                                    <th>SĐT</th>
                                    <th>Địa chỉ</th>
                                    <th>Email</th>
                                    <th>Loại tài khoản</th>
                                    <th>Ngày tạo</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($listUser as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{ $item['name']}}</td>
                                        <td>{{ $item['phone']}}</td>
                                        <td>{{ $item['address']}}</td>
                                        <td>{{ $item['email']}}</td>
                                        <td>{{ $item['role_name']}}</td>
                                        <td>
                                            {{date('d-m-Y',strtotime($item['created_at']))}}
                                        </td>
                                        <td>
                                            <a href="{{route('user.update',$item['id'])}}" class="btn btn-warning"><i class="fa fa-edit pd-1"></i> </a>
                                            <a href="{{route('user.delete',$item['id'])}}" class="btn btn-danger delete_order">Xóa</a>
                                            @if($item['role'] != 5)
                                                <a href="{{route('user.resetPassword',$item['id'])}}" class="btn btn-info duyet_order">Reset mật khẩu</a>
                                            @endif
                                        </td>
                                    </tr> 
                                    @endforeach
                                
                            </tbody>
                        </table>
                        <div class="pagination_page mt-2" style="float: right">
                            <nav>
                                {{ $listUser->appends(Request::all())->links() }}
                            </nav>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
{{-- </div> --}}
@endsection
@section('scripts')
    <script>
        function CreateProductDetail(e) {
            var url = $(e).attr('href');
            
            var Ids = [];
            console.log(url);
            $('#create_product_detail').attr('action', url);
            return false;
        }
        $('.delete_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn Xóa?");
            if (x)
                return true;
            else
                return false;
        });
        $('.duyet_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn reset mật khẩu?");
            if (x)
                return true;
            else
                return false;
        });
    </script>
    @endsection

