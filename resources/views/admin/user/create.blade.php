@extends('layouts.app-new')
@section('title-page', 'Thêm mới tài khoản' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Họ tên</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input name="name" placeholder="Nhập họ tên" value="" type="text" autocomplete="off"
                                                class="form-control" required>
                                            @if ($errors->has('name'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <label class="">Số điện thoại</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="text"  placeholder="Nhập SĐT" value="" autocomplete="off"  title="Nhập SĐT" name="phone" class="form-control"  required>
                                                @if ($errors->has('phone'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('phone') }}</strong></span>
                                                @endif
                                            </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Địa chỉ</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập địa chỉ" value="" autocomplete="off"  title="Nhập địa chỉ" name="address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">email</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập email" value="" autocomplete="off"  title="Nhập email" name="email" class="form-control" >
                                            @if ($errors->has('email'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Mật khẩu</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="password"  placeholder="Nhập mật khẩu" value="" autocomplete="off"  title="Nhập mật khẩu" name="password" class="form-control" >
                                            @if ($errors->has('password'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('password') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Loại tài khoản</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                           
                                            <select  class="form-control select2" id="role"
                                                name="role" >
                                                <option value="">--Chọn loại tài khoản--</option>
                                                @foreach($ls_role as $r)
                                                        <option value="{{$r->id}}"
                                                        >{{$r->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                    <a href="{{route('user.index')}}" class="mt-2 btn btn-danger text-white"><i class="fa fa-arrow-circle-o-left"></i>
                                        Trở về</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
