@extends('layouts.app-new')
@section('title-page', 'Tổng quan website' )
@section('content')
<div class="row">
    <div class="col-md-6 col-xl-4 dash_board">
        <a href="{{route('order.index')}}">
            <div class="card  widget-content  bg-midnight-bloom">
                <div class="widget-content-wrapper background_order">
                    <div class="widget-content-left font_dash">
                        <div class="widget-heading font_text opacity total_order">
                        Tổng đơn hàng
                        <p class="quantity_order">{{$data['ls_order']}}</p>
                    </div>
                        <div class="widget-subheading opacity">
                            <i class="fa fa-cart-plus icon_order"></i>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </a>

    </div>

    <div class="col-md-6 col-xl-4 dash_board">
        <a href="{{route('product.index')}}">
            <div class="card mb-3 widget-content bg-arielle-smile">
                <div class="widget-content-wrapper background_product">
                    <div class="widget-content-left font_dash">
                        <div class="widget-heading font_text opacity total_product">
                            Tổng sản phẩm
                            <p class="quantity_product">{{$data['ls_product']}}</p>
                        </div>
                        <div class="widget-subheading opacity">
                            <i class="fa fa-archive icon_product"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>
    <div class="col-md-6 col-xl-4 dash_board">
        <a href="{{route('post.index')}}">
            <div class="card mb-3 widget-content bg-grow-early">
                <div class="widget-content-wrapper background_post">
                    <div class="widget-content-left font_dash">
                        <div class="widget-heading font_text opacity total_post">
                            Tổng bài viết
                            <p class="quantity_post">{{$data['ls_post']}}</p>
                        </div>
                        <div class="widget-subheading opacity">
                            <i class="fa fas fa-th icon_post "></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>
</div>

@endsection