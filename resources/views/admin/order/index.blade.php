@extends('layouts.app-new')
@section('title-page', 'Danh sách đơn hàng' )
@section('content')
{{-- <div class="container"> --}}
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
            <div class="main-card card">
                <div class="card-body">
                    <form action="" id="search_lucky" method="get">
                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                <input type="text" name="name" id="name" value="{{request('name')}}" placeholder="Tên khách hàng" class="form-control">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <input type="text" name="phone" id="phone" value="{{request('phone')}}" placeholder="Số điện thoại" class="form-control">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <?php
                                        $type_buy = '';
                                        if(isset($_GET['type_buy']))
                                        {
                                            $type_buy = $_GET['type_buy'];
                                        }
                                ?>
                                <select class="form-control" id="type_buy" name="type_buy">
                                        <option value="">--Kiểu đơn hàng--</option>
                                        <option value="1" @if(isset($type_buy) && $type_buy == 1) selected @endif>Online</option>
                                        <option value="2" @if(isset($type_buy) && $type_buy == 2) selected @endif>Offline</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <?php
                                        $category_id = '';
                                        if(isset($_GET['category_id']))
                                        {
                                            $category_id = $_GET['category_id'];
                                        }
                                ?>
                                <select class="form-control select2" id="category_id" name="category_id">
                                <option value="">--Chọn loại sp--</option>
                                @foreach($category as $c)
                                        <option value="{{$c->id}}"
                                        >{{$c->name.'-'.$c->parent_name}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-md-4 col-lg-4 offset-md-1 mt-2">
                            <?php
                                $from_date = '';
                                if(isset($_GET['from_date']))
                                {
                                    $from_date = trim($_GET['from_date']);
                                }
                            ?>
                                <input type="date" placeholder="Từ ngày" class="form-control" name="from_date" id="from_date" value="@if($from_date != ''){{$from_date}}@endif" max="{{ date("Y-m-d") }}">
                            </div>
                            <div class="col-md-4 col-lg-4 offset-2 mt-2">
                            <?php
                                $to_date = '';
                                if(isset($_GET['to_date']))
                                {
                                    $to_date = trim($_GET['to_date']);
                                }
                            ?>
                                <input type="date" placeholder="Đến ngày" class="form-control" name="to_date" id="to_date" value="@if($to_date != ''){{$to_date}}@endif" max="{{ date("Y-m-d") }}">
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 text-center mt-2">
                            <button type="submit" class="btn btn-primary mr-2 search"><i class="fa fa-search"></i> Tìm kiếm</button>
                        </div>
                    </form>
                    <div class="row mt-2">
                        <div class="col-md-6 col-lg-6">
                            <h4>Tổng số đơn hàng: {{count($listOrder)}}</h4>
                            <h4>Tổng doanh thu: {{number_format($total_money)}} VNĐ</h4>
                        </div>
                        @if(Auth::user()->role != 1)
                        <div class="col-md-6 col-lg-6 text-right">
                            <a href="{{route('order.create')}}" class="btn btn-success text-white">
                                <i class="fa fa-plus"></i>
                                Thêm mới
                            </a>
                            <button class="btn btn-primary"><i class="fas fa-download mr-2"></i>Xuất báo cáo</button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách đơn hàng</h5>
                    <div class="table-responsive">
                        <table id="myTable" class="mb-0 table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Họ tên</th>
                                    <th>SĐT</th>
                                    <th>Địa chỉ</th>
                                    <th>Tổng tiền</th>
                                    <th>TT mua</th>
                                    <th>TT Duyệt</th>
                                    <th>TT Giao hàng</th>
                                    <th>Sản phẩm</th>
                                    <th>Ngày tạo</th>
                                    @if(Auth::user()->role != 1)
                                        <th>Thao tác</th>
                                    @else <th>Nhân viên tạo</th> 
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($listOrder as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{ $item['name']}}</td>
                                        <td>{{ $item['phone']}}</td>
                                        <td>{{ $item['address']}}</td>
                                        <td>{{ number_format($item['total_money'])}}</td>
                                        <td>
                                            @if($item['type_buy'] == 1)
                                                Online
                                            @else
                                            <span class="text_null">Offline</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item['status'] == 0)
                                                Chưa duyệt
                                            @else
                                            <span class="text_null">Đã duyệt</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item['delivery_status'] == 0)
                                                Chưa giao
                                            @else
                                            <span class="text_null">Đã giao</span>
                                            @endif
                                        </td>
                                        <td>
                                            <?php 
                                                $ls_pd = \App\Entity\Order_detail::select('p.name','pd.size')
                                                                                ->leftjoin('product_detail as pd','pd.id','order_detail.product_detail_id')
                                                                                ->leftjoin('product as p','p.id','pd.product_id')
                                                                                ->where('order_id',$item['id'])->get();
                                                $str = '';
                                                $dem = 1;
                                                foreach ($ls_pd as $ls) {
                                                    if($dem == count($ls_pd)){
                                                        $str .= $ls->name.'- Size:'.$ls->size;
                                                    }
                                                    else{
                                                        $str .= $ls->name.'- Size:'.$ls->size.'|';
                                                    }
                                                    $dem++;
                                                }
                                            ?>
                                            {{$str}}
                                        </td>
                                        <td>
                                            {{date('d-m-Y',strtotime($item['created_at']))}}
                                        </td>
                                        @if(Auth::user()->role != 1)
                                        <td>
                                            <a href="{{route('order.update',$item['id'])}}" class="btn btn-warning"><i class="fa fa-edit pd-1"></i> </a>
                                            <a href="{{route('order.delete',$item['id'])}}" class="btn btn-danger delete_order">Hủy</a>
                                            @if($item['status'] == 0)
                                                <a href="{{route('order.check_order',$item['id'])}}" class="btn btn-info duyet_order">Duyệt</a>
                                            @endif
                                        </td>
                                        @else <td>{{$item['staff_name']}}</td>
                                        @endif
                                    </tr> 
                                    @endforeach
                                
                            </tbody>
                        </table>
                        <div class="pagination_page mt-2" style="float: right">
                            <nav>
                                {{ $listOrder->appends(Request::all())->links() }}
                            </nav>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
{{-- </div> --}}
@endsection
@section('scripts')
    <script>
        function CreateProductDetail(e) {
            var url = $(e).attr('href');
            
            var Ids = [];
            console.log(url);
            $('#create_product_detail').attr('action', url);
            return false;
        }
        $('.delete_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn hủy?");
            if (x)
                return true;
            else
                return false;
        });
        $('.duyet_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn duyệt?");
            if (x)
                return true;
            else
                return false;
        });
    </script>
    @endsection

