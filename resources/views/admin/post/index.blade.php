@extends('layouts.app-new')
@section('title-page', 'Danh sách bài viết' )
@section('content')
{{-- <div class="container"> --}}
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
            <div class="main-card card">
                <div class="card-body">
                    <form action="" id="search_lucky" method="get">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <input type="text" name="title" id="title" value="{{request('title')}}" placeholder="Tiêu đề" class="form-control">
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <button type="submit" class="btn btn-primary mr-2 search"><i class="fa fa-search"></i> Tìm kiếm</button>
                            </div>
                        </div>
                    </form>
                    <div class="row mt-2">
                        <div class="col-md-6 col-lg-6">
                            <h4>Tổng số bài viết: {{count($post)}}</h4>
                        </div>
                        @if(Auth::user()->role != 1)
                        <div class="col-md-6 col-lg-6 text-right">
                            <a href="{{route('post.create')}}" class="btn btn-success text-white">
                                <i class="fa fa-plus"></i>
                                Thêm mới
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách bài viết</h5>
                    <div class="table-responsive">
                        <table id="myTable" class="mb-0 table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tiêu đề</th>
                                    <th>Ảnh</th>
                                    <th>Loại bài viết</th>
                                    <th>Ngày tạo</th>
                                    @if(Auth::user()->role != 1)
                                    <th>Thao tác</th>
                                    @else <th>Nhân viên tạo</th> 
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($post as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{ $item['title']}}</td>
                                        <td>
                                            @if(isset($item['image']))
                                            <img src="{{asset($item['image'])}}" width="80" height="70"/>
                                            @else
                                            <span class="text_null">{{'Chưa cập nhật'}}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item['type'] == 1)
                                                Giới thiệu
                                            @else
                                            <span class="text_null">Tin tức</span>
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y',strtotime($item['created_at']))}}</td>
                                        @if(Auth::user()->role != 1)
                                        <td>
                                            <a href="{{route('post.update',$item['id'])}}" class="btn btn-warning"><i class="fa fa-edit pd-1"></i> </a>
                                            <a href="{{route('post.delete',$item['id'])}}" class="btn btn-danger delete_product"><i class="fa fa-trash pd-1"></i> </a>
                                        </td>
                                        @else <td>{{$item['staff_name']}}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="pagination_page mt-2" style="float: right">
                            <nav>
                                {{ $post->appends(Request::all())->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form role="form" action=""  method="POST" id="create_product_detail" >
                {!! csrf_field() !!}
          <!-- Modal content-->
                <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm mới</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Hình ảnh </label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="file" required accept="image/*"
                                    onchange="preview_image(event, 'image')" 
                                    name="image">
                                <img width="100px" class="mt-2"  id="image">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Size</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập size" value="" autocomplete="off"  title="Nhập size" name="size" class="form-control">
                            </div>
                        </div>
                        
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Số lượng</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập số lượng" value="" autocomplete="off"  title="Nhập số lượng" name="total_product" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary send1">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
{{-- </div> --}}
@endsection
@section('scripts')
    <script>
        function CreateProductDetail(e) {
            var url = $(e).attr('href');
            
            var Ids = [];
            console.log(url);
            $('#create_product_detail').attr('action', url);
            return false;
        }
        $('.delete_product').click(function(){
            var x = confirm("Bạn có chắc chắn muốn xóa?");
            if (x)
                return true;
            else
                return false;
        });
        $('.tat_hot').click(function(){
            var x = confirm("Bạn có chắc chắn muốn tắt HOT?");
            if (x)
                return true;
            else
                return false;
        });
        $('.bat_hot').click(function(){
            var x = confirm("Bạn có chắc chắn muốn bật HOT?");
            if (x)
                return true;
            else
                return false;
        });
        $('.tat_sale').click(function(){
            var x = confirm("Bạn có chắc chắn muốn tắt sale?");
            if (x)
                return true;
            else
                return false;
        });
        $('.bat_sale').click(function(){
            var x = confirm("Bạn có chắc chắn muốn bật sale?");
            if (x)
                return true;
            else
                return false;
        });
    </script>
    @endsection

