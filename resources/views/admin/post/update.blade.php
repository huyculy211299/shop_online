@extends('layouts.app-new')
@section('title-page', 'Cập nhật bài viết' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('post.edit',$post->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 ">
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Tiêu đề</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <input name="title" placeholder="Tiêu đề" value="{{$post->title}}" type="text" autocomplete="off"
                                                class="form-control" required>
                                            @if ($errors->has('title'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('title') }}</strong></span>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Loại bài viết</label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <select class="form-control" id="type" name="type">
                                                <option value="2" @if($post->type == 2) selected @endif>Tin tức</option>
                                                <option value="1" @if($post->type == 1) selected @endif>Giới thiệu</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="row mt-2">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <label class="">Hình ảnh </label>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <input type="file" accept="image/*"
                                                onchange="preview_image(event, 'image')" 
                                                name="image">
                                            <img src="{{asset($post->image)}}" width="100px" class="mt-2"  id="image">
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
                                    
                                        <label class="">Nội dung</label>
                                        <textarea placeholder="Nhập nội dung" rows="5" cols="80" name="content" id="ckeditor" class="form-control">{{$post->content}}</textarea>
                                        @if ($errors->has('content'))
                                        <span
                                            class="help-block text-danger "><strong>{{ $errors->first('content') }}</strong></span>
                                        @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                    <a href="{{route('post.index')}}" class="mt-2 btn btn-danger text-white"><i class="fa fa-arrow-circle-o-left"></i>
                                        Trở về</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
