@extends('layouts.app-new')
@section('title-page', 'Thêm mới danh mục con' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('category.store_category')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Tên danh mục con</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input name="name" placeholder="Tên danh mục con" value="" type="text" autocomplete="off"
                                                class="form-control" required>
                                            @if ($errors->has('name'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Danh mục cha</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                           
                                            <select  class="form-control select2" id="category_id"
                                                name="category_id" >
                                                <option value="">--Chọn Danh mục cha--</option>
                                                @foreach($category as $c)
                                                        <option value="{{$c->id}}"
                                                        >{{$c->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                    <a href="{{route('category.index')}}" class="mt-2 btn btn-danger text-white"><i class="fa fa-arrow-circle-o-left"></i>
                                        Trở về</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
