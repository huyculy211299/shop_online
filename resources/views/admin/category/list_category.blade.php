@extends('layouts.app-new')
@section('title-page', 'Danh sách danh mục con' )
@section('content')
{{-- <div class="container"> --}}
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
            <div class="main-card card">
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="col-md-6 col-lg-6">
                            <h4>Tổng số danh mục: {{count($category)}}</h4>
                        </div>
                        <div class="col-md-6 col-lg-6 text-right">
                            <a href="{{route('category.create')}}" class="btn btn-success text-white">
                                <i class="fa fa-plus"></i>
                                Thêm Danh mục con
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
      <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form role="form" action=""  method="POST" id="update_category">
                {!! csrf_field() !!}
          <!-- Modal content-->
                <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                        <h4 class="modal-title">Sửa danh mục</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Tên danh mục</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập tên danh mục"  name="name" id="name_category" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary send1">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách danh mục</h5>
                    <div class="table-responsive">
                        <table id="myTable" class="mb-0 table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Danh mục cha</th>
                                    <th>Tên danh mục</th>
                                    <th>Ngày tạo</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($category as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$parent_category->name}}</td>
                                        <td>{{ $item['name']}}</td>
                                        <td>
                                            {{date('d-m-Y',strtotime($item['created_at']))}}
                                        </td>
                                        <td>
                                            <button href="{{route('category.edit',$item['id'])}}" name="{{ $item['name']}}" type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal2" onclick="return UpdateCategory(this);">Sửa</button>
                                            <a href="{{route('category.delete',$item['id'])}}" class="btn btn-danger delete_order">Xóa</a>
                                        </td>
                                    </tr> 
                                    @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
{{-- </div> --}}
@endsection
@section('scripts')
    <script>
        function UpdateCategory(e) {
            var url = $(e).attr('href');
            var name = $(e).attr('name');
            var Ids = [];
            console.log(url);
            $('#update_category').attr('action', url);
            $('#name_category').attr('value', name);
            return false;
        }
        $('.delete_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn xóa?");
            if (x)
                return true;
            else
                return false;
        });
        $('.duyet_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn duyệt?");
            if (x)
                return true;
            else
                return false;
        });
    </script>
    @endsection

