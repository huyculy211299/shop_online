
<div class="message-wrapper">
    <ul class="messages">
        <li class="message clearfix ">
            <div class="formCreatedQuestion" id="{{ $chatRoom['author_id'] }}">
                <p>Họ và tên: {{ $chatRoom['author_name'] }}</p>
                <p>E-mail: {{ $chatRoom['author_email'] }}</p>
                <p>SĐT: {{ $chatRoom['author_phone'] }}</p>
                <p>Nội dung câu hỏi: {{ $chatRoom['message'] }}</p>
                <p class="date"> {{$chatRoom['created_at']}}</p>
            </div>
        </li>
    </ul>
</div>
@if (!empty($messages))
<div class="message-wrapper">
    <ul class="messages">
        @foreach ($messages as $message)
        <li class="message clearfix">
        <div class="{{ $message->user_chat_id == $message->auth_id ? 'sent' :'received'}}">
                <p>{{$message->message}}</p>
                <p class="date"> {{date('d M y, h:i a',strtotime($message->created_at))}}</p>
            </div>
        </li>
        @endforeach

    </ul>
</div>
@endif

<div class="input-text input-sendMessage" id="{{ $chatRoom['author_id'] }}">
    <input type="text" name="message" autocomplete="off" class="submit form-control">
</div>

