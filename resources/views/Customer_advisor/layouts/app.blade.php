<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
    <link href="{{ asset('css/chatbox.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
  
      <iframe style="display: none; position: fixed; z-index: 999999; border: 0px; overflow: hidden; width: 350px; height: 500px; bottom: 0px; right: 0px; margin-right: 2px;"  id="pageChat" src="" frameborder="0">

    </iframe>

    <script src="{{asset('js/jquery-v3.5.1.js')}}"></script>
    <script src="{{asset('js/socket-io-v2.3.js')}}" ></script>
    <script src="{{asset('js/date-format.js')}}"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js" ></script> --}}
    @yield('after-scripts')

    <script>
        $(document).ready(function(){
    
            // chat riêng
            var port = 9696;
            var socket = io('http://localhost:' +port);
                socket.on('laravel_database_chat_user_message:message',function(message){
                    console.log(message);
                    var auth = '{{Auth::id()}}';
                    var typeMessage = message.user_chat_id != message.auth_id ?'sent':'received';
                    var logMessage='';
                    logMessage += '<li class="message clearfix">';
                    logMessage += '<div class="'+typeMessage+'">';
                    logMessage += '<p>'+message.message+'</p>';
                    logMessage += '<p class="date">'+ dateFormat(message.created_at)+'</p>';
                    logMessage += '</div>';
                    logMessage += '</li>';
                    // console.log(logMessage);
                    $('li.message:last-child').after(logMessage);
                    scrollToBottomMessage();
    
                })
    
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // Enable pusher logging - don't include this in production
    
            $('.user').click(function(){
                $('.user').removeClass('active');
                $(this).children().prev().css('display','none');
                console.log($(this).children().prev());
                $(this).addClass('active');
                var user_chat_id = $(this).attr('id');
                // console.log(user_chat_id);
                $.ajax({
                    type: 'get',
                    url: "getListMessageUser"  ,//need to create this route
                    data: {
                        user_chat_id:user_chat_id
                    },
                    cache:false,
                    success:function(data){
                        console.log(data);
                        $('#messages').html(data);
                        scrollToBottomMessage();
                        $('.input-text input.submit').focus();
                    }
                });
    
            });
            $(document).on('keyup','.input-text input',function(e){
                var message = $(this).val();
                //check if enter key is pressed and message id not null also receiver is selected
                var user_chat_id = $('.user.active').attr('id');
                if(e.keyCode == 13 && message != ''){
                    $(this).val('');//prossed enter text box will be empty
                    $.ajax({
                        type: 'post',
                        url: 'adminsendmessage',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            message: message,
                            user_chat_id:user_chat_id
                        },
                        cached:false,
                        success: function(data){
                            console.log(data);
    
                            // $('li.message:last-child').after(data);
                            // scrollToBottomMessage();
    
                        },
                        error:function(jqXHR,status,error){
    
                        },
                        complete:function(){
    
                        }
                    });
                }
            });
            function scrollToBottomMessage(){
                $('.message-wrapper').animate({
                    scrollTop:$('.message-wrapper').get(0).scrollHeight
                },500);
            }
        });
        $(document).on('click','#chatboxImage',function(){
            var urlPageChat = '{{url("requestCustomer")}}';
            console.log(urlPageChat);
            var iframe=  document.getElementById("pageChat");
            iframe.setAttribute('src',urlPageChat);
            iframe.style.display='block';
            // $('#createChatBox').show();

        });
    </script>
</body>
</html>
