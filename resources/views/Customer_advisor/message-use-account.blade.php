<style>
.border-content-chat-box .message-wrapper {
    border: 1px solid #188b86;
}
.message-wrapper {
    padding: 10px;
    height: 280px;
    background: #cecece;
}
.user-wrapper, .message-wrapper {
    border: 1px solid #dddddd;
    overflow-y: auto;
}
input[type=text] {
    width: 100%;
    display: inline-block;
    border-radius: 4px;
    box-sizing: border-box;
    outline: none;
    border: 1px solid #348a80;
}
.form-control {
    height: 31px;
}
.messages .message {
    margin-bottom: 15px;
}
.input-sendMessage {
    margin-bottom: 10px;
    margin-top: 5px;
}
.formCreatedQuestion {
    width: 100%;
    padding: 3px 10px;
    border-radius: 10px;
    /* float: right; */
    margin: 0 auto;
    background: #3bebff;
}

.received, .sent {
    width: 65%;
    padding: 3px 10px;
    border-radius: 10px;
}
.received {
    background: #fff;
}
.sent {
    background: #3bebff;
    float: right;
    text-align: left;
}
p {
    margin: 0 0 5px 0!important;
}
</style>
<div class="container-fluid border-content-chat-box" style="font-size: 12px">
    <div class="message-wrapper">
        <ul class="messages">
            <li class="message clearfix ">
                <div class="formCreatedQuestion" id="{{ $user->id }}">
                    <p>Xin chào: {{ $user->name }}</p>
                    <p>Chúng tôi rất hân hạnh được phục vụ bạn</p>
                    <p class="date"> {{date('d M y, h:i a',strtotime($now))}}</p>
                </div>
            </li>
        </ul>
    </div>
    
    <div class="input-text input-sendMessage" id="{{ $user->id }}">
        <input type="text" name="message" autocomplete="off" class="submit form-control">
    </div>
</div>
