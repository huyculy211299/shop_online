<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Tư vấn khách hàng</title>
    <link  href="{{asset('css/cus_general.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/chatbox.css')}}">
</head>

<body>
    <div class="container-fluid border-content-chat-box" id="contenChatCalled">
      {{-- <div class=" position-relative">
          <h4 class="title-chatbox">Chào mừng bạn đã đến với dịch vụ tư vấn trực tuyến của công ty!</h4>
          <div class="closeChatMessage position-absolute" onclick="getElementById('contenChatCalled').style.display='none'">
            X
          </div>
      </div> --}}
        {{-- <form action="" method="post"> --}}
        <div class="row">
          <div class="col-12">
            <div id="formCreateQuestion">
              <div class="form-group">
                <label for="inputFullName">Họ và Tên <span class="text-danger">*</span></label
                >
                <input
                  autocomplete="off"
                  type="text"
                  class="form-control"
                  id="inputFullName"
                  aria-describedby="emailHelp"
                  placeholder="Nhập vào họ và tên"
                />
              </div>
              <div class="form-group">
                <label for="inputEmail"
                  >Email <span class="text-danger">*</span></label
                >
                <input
                  autocomplete="off"
                  type="email"
                  class="form-control"
                  id="inputEmail"
                  aria-describedby="emailHelp"
                  placeholder="Nhập vào E-mail"
                />
              </div>
              <div class="form-group">
                <label for="inputPhonenumber"
                  >Số điện thoại <span class="text-danger">*</span></label
                >
                <input
                  autocomplete="off"
                  type="text"
                  class="form-control"
                  id="inputPhonenumber"
                  onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                  maxlength="10"
                  placeholder="Nhập vào số điện thoại"
                />
              </div>
              <div class="form-group">
                <label for="inputContentQuestion"
                  >Nội dung câu hỏi <span class="text-danger">*</span></label
                >
                <textarea
                  class="form-control-textarea"
                  id="inputContentQuestion"
                  rows="5"
                  placeholder="Nhập vào nội dung câu hỏi..."
                ></textarea>
              </div>

              <div class="form-group text-right">
                <button
                  class="btn bg-orange text-white sendMessage"  style="font-size: 10px"
                  onclick="createContact();" 
                >
                  Gửi câu hỏi
                </button>
              </div>
            </div>
          </div>
        </div>
      {{-- </form> --}}
      <div id="showCreateQuestion"></div>
    </div>
    <script src="{{asset('js/jquery-v3.5.1.js')}}"></script>
    <script src="{{asset('js/socket-io-v2.3.js')}}" ></script>
    <script src="{{asset('js/date-format.js')}}"></script>

    <script>
       
        function scrollToBottomMessage(){
            $('.message-wrapper').animate({
                scrollTop:$('.message-wrapper').get(0).scrollHeight
            },500);
        }
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var port = 9696;
            var socket = io('http://localhost:'+port);
            socket.on('laravel_database_chat_user_message:message',function(message){
                console.log(message);
                var auth = '{{Auth::id()}}';
                console.log(auth);
                var token = "{{ csrf_token() }}";
                var typeMessage = message.customer_token == token ?'sent':'received';
                // var typeMessage = message.user_chat_id == message.auth_id ?'sent':'received';
                var logMessage='';
                logMessage += '<li class="message clearfix">';
                logMessage += '<div class="'+typeMessage+'">';
                logMessage += '<p>'+message.message+'</p>';
                logMessage += '<p class="date">'+ dateFormat(message.created_at)+'</p>';
                logMessage += '</div>';
                logMessage += '</li>';
                // console.log(logMessage);
                $('li.message:last-child').after(logMessage);
                scrollToBottomMessage();

            })
        });

       
        function createContact(){
            let inputFullName = $('#inputFullName').val();
            let inputEmail = $('#inputEmail').val();
            let inputPhonenumber = $('#inputPhonenumber').val();
            let inputContentQuestion = $('#inputContentQuestion').val();
            var checkverify = true;
            if(inputFullName ==''){
                checkverify = false;
                // $('#inputFullName').after('<p class="color-red">Bạn chưa điền vào nội dung này</p>');
                $('#inputFullName').css('border','1px solid red');
                $('#inputFullName::placeholder').css('color','red');
                $("input::-webkit-input-placeholder").css({"color" : "#b2cde0"});
            }
            if(inputEmail ==''){
                checkverify = false;
                // $('#inputEmail').after('<p class="color-red">Bạn chưa điền vào nội dung này</p>');
                $('#inputEmail').css('border','1px solid red');
            }
            if(inputPhonenumber ==''){
                checkverify = false;
                // $('#inputPhonenumber').after('<p class="color-red">Bạn chưa điền vào nội dung này</p>');
                $('#inputPhonenumber').css('border','1px solid red');

            }
            if(inputContentQuestion ==''){
                checkverify = false;
                // $('#inputContentQuestion').after('<p class="color-red">Bạn chưa điền vào nội dung này</p>');
                $('#inputContentQuestion').css('border','1px solid red');
                $('#inputContentQuestion::placeholder').css('color','red');
                $("input::-webkit-input-placeholder").css({"color" : "red"});

            }
            if(checkverify){
                console.log(checkverify);
                // if (typeof(Storage) !== 'undefined') {
                //     localStorage.setItem('customer_session', inputPhonenumber);
                //     var customer_session = localStorage.getItem('customer_session');
                //     console.log(customer_session);
                // } else {
                //     alert('Trình duyệt của bạn không hỗ trợ Storage');
                // }
                $.ajax({
                    type: 'post',
                    url:'/storetCustomer',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        inputFullName: inputFullName,
                        inputEmail: inputEmail,
                        inputPhonenumber: inputPhonenumber,
                        inputContentQuestion: inputContentQuestion,
                        is_account: 0,

                    },
                    cached:false,
                    success: function(data){
                        console.log(data);
                        $('#formCreateQuestion').hide();
                        $('#showCreateQuestion').after(data);

                    },
                    error:function(jqXHR,status,error){

                    },
                    complete:function(){

                    }
                });
            }
        }
       
        $(document).on('keyup','.input-text input',function(e){
            var message = $(this).val();
            var user_id = $(this).parent().attr('id');
            console.log(message);
            if(e.keyCode == 13 && message != '' ){
                $(this).val('');//prossed enter text box will be empty
                $.ajax({
                    type: 'post',
                    url: 'requestmessage',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        message: message,
                        user_id: user_id,
                        is_account: 1,

                    },
                    cached:false,
                    success: function(data){
                    console.log("data", data)
                        
                        $('li.message:last-child').after(data);
                        scrollToBottomMessage();

                    },
                    error:function(jqXHR,status,error){

                    },
                    complete:function(){

                    }
                });
            }
            console.log(message);
        });
     
</script>
  </body>
</html>