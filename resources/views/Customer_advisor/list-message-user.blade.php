@extends('Customer_advisor.layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="user-wrapper">
                <ul class="users">
                    @if ($listUser->count() >0)
                    @foreach ($listUser as $user)
                    <li class="user" id="{{$user->id}}">
                         @php
                             $countMessage = App\ChatUserMessage::getTotalMessage($user->id);
                         @endphp
                         @if ($countMessage > 0)
                        <span class="pending"> {{$countMessage}}</span>
 
                         @endif
                        <div class="media">
                            <div class="media-left">
                                <img src="{{asset('assets/images/avatars/defaultAvatar.png')}}" alt="" class="media-object">
                            </div>
                            <div class="media-body">
                                <p class="name">{{$user->name}}</p>
                                <p class="email">{{$user->email}}</p>
                                <p class="email">{{$user->phone}}</p>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    @endif
 
 
 
                </ul>
            </div>
        </div>
        <div class="col-md-8" id="messages">
 
        </div>
 
    </div>
 </div>
@endsection
